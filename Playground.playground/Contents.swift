//: Playground - noun: a place where people can play

import UIKit

var string = "Hello, playground"
var number = 15

number + 1
number - 1
number * 2
number / 3
number % 4

(number + 2) * 3

true && true
true && false
false && false

true || true
true || false
false || false

!true
!false

1 & 2
1 | 2
1 ^ 2
~1

string + " OK!"

number += 1  // number = number + 1
number -= 1  // number = number - 1
number *= 2  // number = number * 2
number /= 3  // number = number / 3
number %= 4  // number = number % 4

string += " OK!" // string = string + " OK!"

number = 15
Float(number) + 1.5
String(number) + "!"
Bool(number)

print("1^2 = \(1*1)")
print("2^2 = \(2*2)")
print("3^2 = \(3*3)")
print("4^2 = \(4*4)")
print("5^2 = \(5*5)")
print("6^2 = \(6*6)")
print("7^2 = \(7*7)")
print("8^2 = \(8*8)")
print("9^2 = \(9*9)")
print("10^2 = \(10*10)")

for index in 1...100 {
  print("\(index)^2 = \(index*index)")
}

var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for index in array {
  print("\(index)^2 = \(index*index)")
}

array = [2, 3, 5, 7, 9, 11, 13, 17, 19, 23]
for index in array {
  print("\(index)^2 = \(index*index)")
}

number = 0
while number < 10 {
  number += 1
  print("while: \(number)^2 = \(number*number)")
}

number = 0
repeat {
  number += 1
  print("repeat: \(number)^2 = \(number*number)")
} while number < 10

number = 10
while number < 10 {
  number += 1
  print("while: \(number)^2 = \(number*number)")
}

number = 10
repeat {
  number += 1
  print("repeat: \(number)^2 = \(number*number)")
} while number < 10

var string1 = "This is a String!"
var string2 = "This is a String!"
var string3 = "Another string!"

string1 == string2
string1 != string2
string1 > string3
string1 < string3

string1 + string3
string1 += string3
string1

number = 2
"\(number)^2 = \(number*number)"

var array1 = Array<Int>()
var array2 = [Int]()
var array3 = [Int](count: 3, repeatedValue: 2)
var array4 = [4, 5, 6, 7]

array3 + array4
array3 += array4

array4.count
array4.isEmpty
array4.append(8)
array4.removeFirst()
array4.removeLast()

var dict1 = Dictionary<String, Int>()
var dict2 = [String: Int]()
var dict3 = ["Hanoi, Vietnam": 7, "Seoul, Korea": 9]

dict3.updateValue(0, forKey: "London, UK")     // dict3["London, UK"] = 0
dict3.updateValue(6, forKey: "Hanoi, Vietnam") // dict3["Hanoi, Vietnam"] = 6
dict3.removeValueForKey("Seoul, Korea")        // dict3["Seoul, Korea"] = nil

dict3
Array(dict3.keys)
Array(dict3.values)

var daysArr = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
print("daysArr has \(daysArr.count) elements")
daysArr.removeLast()
daysArr.append("Weekend")
for day in daysArr {
  print(day)
}

var daysDict = ["Mon": 1, "Tue": 2, "Wed": 3, "Thu": 4, "Fri": 5, "Sat": 6]
daysDict.updateValue(0, forKey: "Sun")
daysDict.removeValueForKey("Wed")
