//
//  AddWordViewController.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class EditWordViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate {
  var word: WordModel?
  
  @IBOutlet fileprivate weak var _txtWord: UITextField!
  @IBOutlet fileprivate weak var _txtDefinition: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    if self.word == nil {
      self.navigationItem.title = "Thêm từ mới"
    } else {
      self.navigationItem.title = "Sửa từ"
    }
    
    popuplateWordData()
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    if identifier == SegueID.CancelEditWord.rawValue {
      return true
    }
    
    if _txtWord.text == nil || _txtWord.text == "" || _txtDefinition.text == nil || _txtDefinition.text == "" {
      return false
    }
    
    if self.word == nil { // Add new word
      let newWord = WordModel(word: _txtWord.text!, definition: _txtDefinition.text)
      WordsStore.allWords.append(newWord)
      
      return true
    }
    
    let checkResult = self.word!.updateWord(_txtWord.text, definition: _txtDefinition.text)
    return checkResult
  }
  
  func popuplateWordData() {
    _txtWord.text = self.word?.word
    _txtDefinition.text = self.word?.definition
  }
  
  // UITextFieldDelegate
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    _txtDefinition.becomeFirstResponder()
    return true
  }
  
  // UITextViewDelegate
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      textView.resignFirstResponder()
      return false
    }
    
    return true
  }
}
