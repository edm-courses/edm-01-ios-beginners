//
//  SearchWordsViewController.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class ListWordsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
  @IBOutlet fileprivate weak var _tblAllWords: UITableView!
  
  fileprivate var _selectedWordIndex: Int = -1
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let destinationVC = segue.destination as! UINavigationController
    let editWordVC = destinationVC.viewControllers.first as! EditWordViewController
    
    if segue.identifier == SegueID.AddWord.rawValue {
      editWordVC.word = nil
    } else if segue.identifier == SegueID.EditWord.rawValue {
      editWordVC.word = WordsStore.allWords[_selectedWordIndex]
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return WordsStore.allWords.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ReuseID.WordCell.rawValue) as! WordCell
    
    cell.updateCell(withData: WordsStore.allWords[indexPath.row])
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    _selectedWordIndex = indexPath.row
    return true
  }
  
  @IBAction func cancelEditWord(_ segue: UIStoryboardSegue) {
  }
  
  @IBAction func doneEditWord(_ segue: UIStoryboardSegue) {
    _tblAllWords.reloadData()
    UserDefaultsHelper.saveAllWordsToUserDefaults()
  }
}
