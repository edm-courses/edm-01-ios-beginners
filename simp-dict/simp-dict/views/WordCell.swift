//
//  WordCell.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class WordCell: UITableViewCell {
  @IBOutlet fileprivate weak var _lblTitle: UILabel!
  @IBOutlet fileprivate weak var _lblSubtitle: UILabel!
  
  func updateCell(withData word: WordModel) {
    _lblTitle.text = word.word
    _lblSubtitle.text = word.definition
  }
}
