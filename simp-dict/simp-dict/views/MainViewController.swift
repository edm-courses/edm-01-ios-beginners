//
//  ViewController.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == SegueID.AddWord.rawValue {
      let destinationVC = segue.destination as! UINavigationController
      let editWordVC = destinationVC.viewControllers.first as! EditWordViewController
      editWordVC.word = nil
      
      return
    }
  }
  
  @IBAction func cancelEditWord(_ segue: UIStoryboardSegue) {
  }
  
  @IBAction func doneEditWord(_ segue: UIStoryboardSegue) {
    UserDefaultsHelper.saveAllWordsToUserDefaults()
  }
}

