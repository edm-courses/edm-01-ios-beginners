//
//  CommonTypes.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation

enum SegueID: String {
  case ListWords = "ListWordsSegueIdentifier"
  case EditWord = "EditWordSegueIdentifier"
  case AddWord = "AddWordSegueIdentifier"
  case CancelEditWord = "CancelEditWordSegueIdentifier"
  case DoneEditWord = "DoneEditWordSegueIdentifier"
}

enum ReuseID: String {
  case WordCell = "WordCellIdentifier"
}
