//
//  WordModel.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 5/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation

class WordModel: NSObject {
  var word: String
  var definition: String
  
  override var description: String {
    get {
      return "<WordModel: Word: \(self.word) - Definition: \(self.definition)"
    }
  }

  init(word: String, definition: String) {
    self.word = word
    self.definition = definition
  }
  
  func updateWord(_ word: String?, definition: String?) -> Bool {
    if word == nil || word == "" || definition == nil || definition == "" {
      return false
    }
    
    (self.word, self.definition) = (word!, definition!)
    return true
  }
}
