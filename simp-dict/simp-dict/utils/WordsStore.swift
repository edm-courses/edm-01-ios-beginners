//
//  WordsStore.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 6/8/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation

struct WordsStore {
//  static var allWords: [WordModel] = [
//    WordModel(word: "phone", definition: "điện thoại"),
//    WordModel(word: "cellphone", definition: "điện thoại di động"),
//    WordModel(word: "application", definition: "ứng dụng"),
//    WordModel(word: "developer", definition: "lập trình viên"),
//    WordModel(word: "programming", definition: "(công việc) lập trình")
//  ]

  // Use NSUserDefaults
  static var allWords: [WordModel] = UserDefaultsHelper.loadWordsFromUserDefaults()
}