//
//  WordsUserDefaults.swift
//  simp-dict
//
//  Created by Ethan Nguyen on 6/16/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation

struct UserDefaultsHelper {
  // Static attributes & methods
  fileprivate static var _userDefaultsKey = "simp-dict-words"
  
  fileprivate static let _initWordsData: [[String: String]]! = [
    ["word": "phone", "definition": "điện thoại"],
    ["word": "cellphone", "definition": "điện thoại di động"],
    ["word": "application", "definition": "ứng dụng"],
    ["word": "developer", "definition": "lập trình viên"],
    ["word": "programming", "definition": "(công việc) lập trình"]
  ]
  
  static func checkAndInitUserDefaults() -> [[String: String]] {
    let userDefaults = UserDefaults.standard
    let dictData = userDefaults.object(forKey: self._userDefaultsKey) as? [[String: String]]
    
    // Some data existed, ignore
    if dictData != nil {
      return dictData!
    }
    
    // No data initialized yet
    // Initialize and return
    userDefaults.set(self._initWordsData, forKey: self._userDefaultsKey)
    return self._initWordsData
  }
  
  static func loadWordsFromUserDefaults() -> [WordModel] {
    let savedWordsData = self.checkAndInitUserDefaults()
    
    return savedWordsData.map({ (word) -> WordModel in
      return WordModel(word: word["word"]!, definition: word["definition"]!)
    })
  }
  
  static func saveAllWordsToUserDefaults() {
    let newWordsData = WordsStore.allWords.map { (word) -> [String: String] in
      ["word": word.word, "definition": word.definition]
    }
    
    let userDefaults = UserDefaults.standard
    userDefaults.set(newWordsData, forKey: self._userDefaultsKey)
  }
}
