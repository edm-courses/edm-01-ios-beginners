//
//  ArticlesStore.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/10/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Alamofire
import AEXML
import SwiftyJSON
import Kanna
import ObjectMapper

typealias DownloadArticlesHandler = (_ errorString: String?) -> ()

class ArticlesStore: NSObject {
  // Singleton initializations
  static var sharedStore = ArticlesStore()
  fileprivate override init() {}
  
  // Multiton initializations
  static var oldStore = ArticlesStore()
  
  var pubDateFormatter: DateFormatter {
    get {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZ"
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      return dateFormatter
    }
  }
  
  var displayDateFormatter: DateFormatter {
    get {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "HH:mm:ss, EEEE, dd/MM/yyyy"
      dateFormatter.locale = Locale(identifier: "vi_VN")
      return dateFormatter
    }
  }

  var articleModels: [MArticle] { get { return _articleModels } }
  var mappedArticles: [OArticle] { get { return _mappedArticles } }
  
  fileprivate var _articleModels: [MArticle] = []
  fileprivate var _mappedArticles: [OArticle] = []
  
  func downloadArticles(_ handler: @escaping DownloadArticlesHandler) {
    Alamofire.request("http://vnexpress.net/rss/tin-moi-nhat.rss", method: .get)
      .responseData { [weak self] (response) in
        if response.response!.statusCode != 200 {
          handler("Data not found!")
          return
        }
        
        switch response.result {
        case .success(let rssData):
          do {
            let rssDoc = try AEXMLDocument(xml: rssData)
            self?._populateArticleModels(fromRss: rssDoc)
            
            let jsonArray = self?._convertRssToDict(fromRss: rssDoc)
            self?._mapArticles(fromJSON: jsonArray!)
            
            handler(nil)
          } catch (let error as NSError) {
            handler(error.userInfo["NSXMLParserErrorMessage"] as? String)
          }
        case .failure(let error):
          handler("error: \(error)")
        }
    }
  }
  
  fileprivate func _convertRssToDict(fromRss rss: AEXMLDocument) -> [[String: AnyObject]] {
    var articles = [[String: AnyObject]]()

    for item in rss.root["channel"]["item"].all! {
      let title = item["title"].string
      let link = item["link"].string
      let pubDate = item["pubDate"].string
      
      let descriptionString = "<p>" + item["description"].string + "</p>"
      let descriptionHtml = Kanna.HTML(html: descriptionString, encoding: String.Encoding.utf8)!
      let description = descriptionHtml.text!

      var img = ""
      
      for imageCss in descriptionHtml.css("img") {
        img = imageCss["src"]!
        
        // Get first result item only
        break
      }
      
      // Build raw data in Dictionary from literal dictionary
      let rawArticleDict = [
        "title": title,
        "link": link,
        "img": img,
        "description": description,
        "pubDate": pubDate
      ]
      articles.append(rawArticleDict as [String : AnyObject])
    }
    
    return articles
  }
  
  fileprivate func _populateArticleModels(fromRss rssDoc: AEXMLDocument) {
    _articleModels.removeAll()
    
    for item in rssDoc.root["channel"]["item"].all! {
      let article = MArticle(fromRss: item)
      _articleModels.append(article)
    }
  }
  
  fileprivate func _populateArticleModels(fromJSON jsonArray: [[String: AnyObject]]) {
    _articleModels.removeAll()
    
    for rawArticle in jsonArray {
      let article = MArticle(withAttributes: rawArticle)
      _articleModels.append(article)
    }
  }
  
  fileprivate func _mapArticles(fromJSON jsonArray: [[String: AnyObject]]) {
    _mappedArticles.removeAll()
    
    let articles: [OArticle]? = Mapper<OArticle>().mapArray(JSONArray: jsonArray)
    _mappedArticles.append(contentsOf: articles!)
  }
}
