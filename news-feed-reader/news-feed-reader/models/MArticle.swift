//
//  MArticle.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/10/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import AEXML
import Kanna

class MArticle: NSObject {
  var title: String
  var subtitle: String
  var articleUrl: URL? = nil
  var imageUrl: URL? = nil
  var pubDate: Date
  
  override init() {
    self.title = ""
    self.subtitle = ""
    self.pubDate = Date()
  }
  
  init(fromRss rss: AEXMLElement) {
    self.title = rss["title"].string
    self.articleUrl = NSURL(string: rss["link"].string) as URL?
    
    let descriptionString = "<p>" + rss["description"].string + "</p>"
    let descriptionHtml = Kanna.HTML(html: descriptionString, encoding: String.Encoding.utf8)!
    self.subtitle = descriptionHtml.text!
    
    var imageUrl = ""
    
    for imageCss in descriptionHtml.css("img") {
      imageUrl = imageCss["src"]!
      
      // Get first result item only
      break
    }
    
    self.imageUrl = NSURL(string: imageUrl) as URL?
    
    let pubDateFormatter = ArticlesStore.sharedStore.pubDateFormatter
    
    if let formattedPubDate = pubDateFormatter.date(from: rss["pubDate"].string) {
      self.pubDate = formattedPubDate
    } else {
      self.pubDate = NSDate() as Date
    }
  }
  
  convenience init(withAttributes attributes: [String: AnyObject]) {
    self.init()
    self.updateData(withAttributes: attributes)
  }
  
  func updateData(withAttributes attributes: [String: AnyObject]) {
    if let newTitle = attributes["title"] {
      self.title = newTitle as! String
    }
    
    if let newSubtitle = attributes["description"] {
      self.subtitle = newSubtitle as! String
    }
    
    if let urlString = attributes["link"] {
      self.articleUrl = URL(string: urlString as! String)
    }
    
    if let urlString = attributes["img"] {
      self.imageUrl = URL(string: urlString as! String)
    }
    
    if let pubDateString = attributes["pubDate"] {
      self.pubDate = ArticlesStore.sharedStore.pubDateFormatter.date(from: pubDateString as! String)!
    }
  }
  
  override var description: String {
    return "<MArticle - Title: \(self.title) - URL: \(self.articleUrl) - PubDate: \(self.pubDate)>"
  }
}
