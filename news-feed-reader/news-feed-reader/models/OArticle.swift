//
//  OArticle.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class OArticle: NSObject, Mappable {
  var title: String?
  var subtitle: String?
  var articleUrl: URL?
  var imageUrl: URL?
  var pubDate: Date?
  
  override var description: String {
    return "<MArticle - Title: \(self.title) - URL: \(self.articleUrl) - PubDate: \(self.pubDate)>"
  }
  
  // Mappable methods
  required init?(map: Map) {
    if map["title"].value() === nil || map["title"].value() == "" ||
      map["link"].value() === nil || map["link"].value() == "" {
      return nil
    }
  }
  
  // Mappable
  func mapping(map: Map) {
    title       <- map["title"]
    subtitle    <- map["description"]
    articleUrl  <- (map["link"], URLTransform())
    imageUrl    <- (map["img"], URLTransform())
    let dateTransform = DateFormatterTransform(dateFormatter: ArticlesStore.sharedStore.pubDateFormatter)
    pubDate     <- (map["pubDate"], dateTransform)
  }
}
