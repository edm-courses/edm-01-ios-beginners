//
//  CustomSFSafariViewController.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import SafariServices

class CustomSFSafariViewController: SFSafariViewController {
  weak var articleModel: MArticle? = nil
  weak var mappedArticle: OArticle? = nil
  
  fileprivate var _oldNavTitleTextAttr: [String: AnyObject]? = nil
  
  convenience init(fromModel article: MArticle) {
    self.init(url: article.articleUrl! as URL, entersReaderIfAvailable: true)
    self.articleModel = article
  }
  
  convenience init(fromMappedModel article: OArticle) {
    self.init(url: article.articleUrl! as URL, entersReaderIfAvailable: true)
    self.mappedArticle = article
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupNavigationBar()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    _oldNavTitleTextAttr = self.navigationController?.navigationBar.titleTextAttributes as [String : AnyObject]?
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Light", size: 13)!
    ]
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.navigationController?.navigationBar.titleTextAttributes = _oldNavTitleTextAttr
  }
  
  // Custom functions
  fileprivate func _setupNavigationBar() {
    if self.navigationController == nil {
      return
    }
    
    if self.articleModel != nil {
      self.navigationItem.title = self.articleModel?.title
    } else if self.mappedArticle != nil {
      self.navigationItem.title = self.mappedArticle?.title
    }
    
    self.navigationItem.hidesBackButton = true
    let customBackButton = UIBarButtonItem(image: UIImage(named: "btn-back")!,
                                           style: .plain,
                                           target: self,
                                           action: #selector(_popViewControllerAnimated))
    self.navigationItem.leftBarButtonItem = customBackButton
  }
  
  @objc fileprivate func _popViewControllerAnimated() {
    _ = self.navigationController?.popViewController(animated: true)
  }
}
