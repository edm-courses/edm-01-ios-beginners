//
//  ArticleCell.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/10/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage
import NSDate_TimeAgo

class ArticleCell: UITableViewCell {
  @IBOutlet weak var _imgArticleImage: UIImageView!
  @IBOutlet weak var _lblTitle: UILabel!
  @IBOutlet weak var _lblSubtitle: UILabel!
  @IBOutlet weak var _lblPubDate: UILabel!
  
  func updateCell(withModel article: MArticle) {
    _imgArticleImage.image = nil
    
    if article.imageUrl != nil {
      _imgArticleImage.af_setImage(withURL: article.imageUrl!)
    }
    
    _lblTitle.text = article.title
    _lblSubtitle.text = article.subtitle
    _lblPubDate.text = ArticlesStore.sharedStore.displayDateFormatter.string(from: article.pubDate)
  }
  
  func updateCell(withMappedModel article: OArticle) {
    _imgArticleImage.image = nil
    
    if article.imageUrl != nil {
      _imgArticleImage.af_setImage(withURL: article.imageUrl!)
    }
    
    _lblTitle.text = article.title
    _lblSubtitle.text = article.subtitle
    _lblPubDate.text = (article.pubDate! as NSDate).timeAgo()
  }
}
