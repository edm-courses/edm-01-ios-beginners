//
//  ViewController.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/10/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import DGElasticPullToRefresh
import Toaster

class NewsFeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet var _tblArticles: UITableView!
  @IBOutlet weak var _btnSwitchMode: UIBarButtonItem!
  
  // Presentation mode for Safari VC:
  // If true: use navigationController?.pushViewController
  // If false: use navigationController?.presentViewController
  fileprivate let _safariViewControllerPushToShow = true
  fileprivate var _useMappedArticles = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupViews()
  }

  // UITableViewDataSource methods
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let sharedStore = ArticlesStore.sharedStore
    return _useMappedArticles ? sharedStore.mappedArticles.count : sharedStore.articleModels.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCellIdentifier") as! ArticleCell
    
    let sharedStore = ArticlesStore.sharedStore
    
    if _useMappedArticles {
      let article = sharedStore.mappedArticles[indexPath.row]
      cell.updateCell(withMappedModel: article)
    } else {
      let article = sharedStore.articleModels[indexPath.row]
      cell.updateCell(withModel: article)
    }
    
    return cell
  }
  
  // UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let sharedStore = ArticlesStore.sharedStore
    var safariVC: CustomSFSafariViewController? = nil
    
    if _useMappedArticles {
      let article = sharedStore.mappedArticles[indexPath.row]
      safariVC = CustomSFSafariViewController(fromMappedModel: article)
    } else {
      let article = sharedStore.articleModels[indexPath.row]
      safariVC = CustomSFSafariViewController(fromModel: article)
    }
    
    if _safariViewControllerPushToShow {
      self.navigationController?.pushViewController(safariVC!, animated: true)
    } else {
      self.navigationController?.present(safariVC!, animated: true, completion: nil)
    }
  }
  
  @IBAction func btnSwitchModeTouchedUpInside(_ sender: UIBarButtonItem) {
    _useMappedArticles = !_useMappedArticles
    _tblArticles.reloadData()
    
    let message = "Dùng \(_useMappedArticles ? "Mapped" : "Normal") Objects làm datasource"
    Toast(text: message, duration: 1).show()
  }
  
  // Custom functions
  fileprivate func _setupViews() {
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    let switchModeTextAttr = [
      NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 25)!,
      NSForegroundColorAttributeName: UIColor.black
    ]
    _btnSwitchMode.setTitleTextAttributes(switchModeTextAttr, for: UIControlState())
    _btnSwitchMode.setTitlePositionAdjustment(UIOffset(horizontal: -10, vertical: 10), for: .default)
    
    self._setupArticlesTableView()
  }
  
  fileprivate func _setupArticlesTableView() {
    var frame = self.view.bounds
    frame.origin.y = 64
    frame.size.height -= 64
    _tblArticles.frame = frame
    self.view.addSubview(_tblArticles)
    
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
    _tblArticles.dg_addPullToRefreshWithActionHandler({ [weak self] in
      self?._downloadArticles()
      }, loadingView: loadingView)
    _tblArticles.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
    _tblArticles.dg_setPullToRefreshBackgroundColor(_tblArticles.backgroundColor!)
//    _tblArticles.dg_startLoading()
  }
  
  fileprivate func _downloadArticles() {
    ArticlesStore.sharedStore.downloadArticles { [weak self] (errorString) in
      self?._tblArticles.dg_stopLoading()
      self?._tblArticles.reloadData()
      
      if errorString != nil {
        Toast(text: "Có lỗi xảy ra!\n\(errorString)", duration: 1).show()
        return
      }
    }
  }
}
