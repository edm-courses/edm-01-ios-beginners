//
//  SongsLibrary.swift
//  music-player-lite
//
//  Created by Ethan Nguyen on 6/8/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation

struct MusicLibrary {
  static let songs: [[String: String]] = [
    [
      "artist": "Francis Goya",
      "title": "Romance De Amour",
      "album": "Top 100",
      "fileName": "Romance-De-Amour"
    ],
    [
      "artist": "John Epping",
      "title": "Summer Leaves",
      "album": "Top 100",
      "fileName": "Summer-Leaves"
    ],
    [
      "artist": "Yiruma",
      "title": "River Flows In You",
      "album": "Top 100",
      "fileName": "River-Flows-In-You"
    ],
    [
      "artist": "Kevin Kern",
      "title": "Sundial Dreams",
      "album": "Top 100",
      "fileName": "Sundial-Dreams"
    ],
    [
      "artist": "Francis Goya",
      "title": "Godfather",
      "album": "Top 100",
      "fileName": "Godfather"
    ],
    [
      "artist": "Richard Clayderman",
      "title": "Music Box Dancer",
      "album": "Top 100",
      "fileName": "Music-Box-Dancer"
    ],
    [
      "artist": "Brian Crain",
      "title": "Canon In D",
      "album": "Top 100",
      "fileName": "Canon-In-D"
    ],
    [
      "artist": "Richard Clayderman",
      "title": "La Vie En Rose",
      "album": "Top 100",
      "fileName": "La-Vie-En-Rose"
    ],
    [
      "artist": "Yiruma",
      "title": "Kiss The Rain",
      "album": "Top 100",
      "fileName": "Kiss-The-Rain"
    ],
    [
      "artist": "Richard Clayderman",
      "title": "Balada Para Adelina",
      "album": "Top 100",
      "fileName": "Balada-Para-Adelina"
    ]
  ]
}
