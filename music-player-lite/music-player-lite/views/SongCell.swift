//
//  SongCell.swift
//  music-player-lite
//
//  Created by Ethan Nguyen on 6/4/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class SongCell : UITableViewCell {
  @IBOutlet weak var _imgAlbumArtwork: UIImageView!
  @IBOutlet weak var _lblTitle: UILabel!
  @IBOutlet weak var _lblArtist: UILabel!
  @IBOutlet weak var _lblAlbum: UILabel!
  
  func updateCell(withData songData: [String: String]) {
    if let artwork = songData["albumArtwork"] {
      _imgAlbumArtwork.image = UIImage(named: artwork)
    }
    
    _lblTitle.text = songData["title"]
    _lblArtist.text = songData["artist"]
    _lblAlbum.text = songData["album"]
  }
}
