//
//  ViewController.swift
//  music-player-lite
//
//  Created by Ethan Nguyen on 6/4/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AVFoundation

class SongsListViewController: UITableViewController, AVAudioPlayerDelegate {
  fileprivate var _audioPlayer: AVAudioPlayer!
  fileprivate var _playbackTimer: Timer!
  fileprivate var _currentSongIndex: Int = 0
  
  @IBOutlet var _vControls: UIView!
  @IBOutlet weak var _lblCurrentSongTitle: UILabel!
  @IBOutlet weak var _lblCurrentSongArtist: UILabel!
  @IBOutlet weak var _btnPlayPause: UIButton!
  @IBOutlet weak var _sldVolume: UISlider!
  @IBOutlet weak var _prgPlaybackProgress: UIProgressView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupViews()
  }
  
  // UITableViewDataSource
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return MusicLibrary.songs.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SongCellIdentifier") as! SongCell
    cell.updateCell(withData: MusicLibrary.songs[indexPath.row])
    return cell
  }
  
  // UITableViewDelegate
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    _currentSongIndex = indexPath.row
    self._playCurrentIndexSong()
  }
  
  override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }
  
  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return _vControls.frame.size.height
  }
  
  // AVAudioPlayerDelegate
  func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    self._togglePlayPauseButton(forcePlay: false)
    
    if flag {
      // Play next song in the list
      self._playRelativeSong(nextSong: true)
    } else {
      print("Something went wrong!")
    }
  }
  
  func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
    print(error ?? "error!")
  }
  
  func audioPlayerBeginInterruption(_ player: AVAudioPlayer) {
    print(player)
  }
  
  func audioPlayerEndInterruption(_ player: AVAudioPlayer, withOptions flags: Int) {
    print(flags)
  }
  
  // Button callbacks
  @IBAction func btnPrevTouchedUpInside(_ sender: UIButton) {
    self._playRelativeSong(nextSong: false)
  }
  
  @IBAction func btnPlayPauseTouchedUpInside(_ sender: UIButton) {
    self._togglePlayPauseButton()
  }
  
  @IBAction func btnNextTouchedUpInside(_ sender: UIButton) {
    self._playRelativeSong(nextSong: true)
  }
  
  @IBAction func sldVolumeValueChanged(_ sender: UISlider) {
    _audioPlayer.volume = sender.value
  }
  
  // Custom functions
  fileprivate func _setupViews() {
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    _sldVolume.setThumbImage(UIImage(named: "img-slider-thumb"), for: UIControlState())
    self._prepareToPlay(song: MusicLibrary.songs.first!)
    var frame = _vControls.frame
    frame.origin.y = (self.navigationController?.view.frame.size.height)! - frame.size.height
    _vControls.frame = frame
    self.navigationController?.view.addSubview(_vControls)
    
    do {
      try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
      try AVAudioSession.sharedInstance().setActive(true)
    } catch (let error as NSError) {
      print("audioSession error: \(error)")
    }
  }
  
  fileprivate func _prepareToPlay(song songData: [String: String]) {
    // Release audio player instance first
    _audioPlayer = nil
    
    let songPath = Bundle.main.path(forResource: songData["fileName"], ofType: "mp3")!
    let songUrl = URL(fileURLWithPath: songPath)
    
    do {
      try _audioPlayer = AVAudioPlayer(contentsOf: songUrl)
      _audioPlayer.delegate = self
      _audioPlayer.volume = _sldVolume.value
      _audioPlayer.prepareToPlay()
    } catch (let error as NSError) {
      print("audioPlayer error: \(error)")
    }
    
    _lblCurrentSongTitle.text = songData["title"]
    _lblCurrentSongArtist.text = songData["artist"]
  }
  
  fileprivate func _playRelativeSong(nextSong isNext: Bool) {
    _currentSongIndex += isNext ? 1 : -1
    
    if _currentSongIndex >= MusicLibrary.songs.count {
      _currentSongIndex = 0
    } else if _currentSongIndex < 0 {
      _currentSongIndex = MusicLibrary.songs.count-1
    }
    
    self._playCurrentIndexSong()
  }
  
  fileprivate func _playCurrentIndexSong() {
    let currentSong = MusicLibrary.songs[_currentSongIndex]
    let selectedIndexPath = IndexPath(row: _currentSongIndex, section: 0)
    self.tableView.selectRow(at: selectedIndexPath, animated: true, scrollPosition: .middle)
    
    self._prepareToPlay(song: currentSong)
    self._togglePlayPauseButton(forcePlay: true)
  }
  
  fileprivate func _togglePlayPauseButton(forcePlay forced: Bool? = nil) {
    if forced != nil {
      // If is forced play, set state as play
      _btnPlayPause.isSelected = forced!
    } else {
      // Toggle Play - Pause state
      // Selected = true: play
      // Selected = false: pause
      _btnPlayPause.isSelected = !_btnPlayPause.isSelected
    }
    
    // Play
    if _btnPlayPause.isSelected {
      _playbackTimer = Timer.scheduledTimer(timeInterval: 0.1,
                                                              target: self,
                                                              selector: #selector(_updateAudioPlaybackProgress),
                                                              userInfo: nil,
                                                              repeats: true)
      _audioPlayer.play()
    } else {
      // Pause
      _playbackTimer.invalidate()
      _playbackTimer = nil
      _audioPlayer.pause()
    }
    
    if self.tableView.indexPathForSelectedRow == nil {
      self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
    }
  }
  
  @objc fileprivate func _updateAudioPlaybackProgress() {
    let progress = _audioPlayer.currentTime / _audioPlayer.duration
    _prgPlaybackProgress.progress = Float(progress)
  }
}
