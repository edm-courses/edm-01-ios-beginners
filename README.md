# Swift 3 migration guide

##1. Syntax:

- Biến đầu tiên trong khai báo hàm phải luôn có tham số, muốn ẩn tham số này khi gọi hàm thì thêm từ khóa `_` vào trước, ví dụ:

    ```diff
    -  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    +  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
         return true
       }
    ```

- Từ khóa `private` đổi thành `fileprivate`:

    ```diff
    -  private func convertLunarToSolar() -> Void {
    +  fileprivate func convertLunarToSolar() -> Void {
         let (day, month, year) = getDayMonthYearFromDatePicker()
         let (solarDay, solarMonth, solarYear) = _lunarDayUtils.convertLunarToSolar(day, lunarMonth: month, lunarYear: year)
    ```

- Các API viết lại cho ngắn gọn hơn:

    + `UserDefault`:

        ```diff
        +    let dictData = userDefaults.objectForKey(self._userDefaultsKey) as? [[String: String]]
        -    let dictData = userDefaults.object(forKey: self._userDefaultsKey) as? [[String: String]]
        +    userDefaults.setObject(self._initWordsData, forKey: self._userDefaultsKey)
        -    userDefaults.set(self._initWordsData, forKey: self._userDefaultsKey)
        ```

    + `UITableView`:

        ```diff
        +  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        -  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return AddressBook.contacts.count
           }

        +  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        +    let cell = tableView.dequeueReusableCellWithIdentifier("ContactListCellIdentifier") as! ContactListCell
        -  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        -    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCellIdentifier") as! ContactListCell
        ```

    + `UICollectionView`

        ```diff
        +  override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        -  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             return AddressBook.contacts.count
           }
           
        +  override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        +    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ContactCardCellIdentifier", forIndexPath: indexPath) as! ContactCardCell
        -  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        -    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCardCellIdentifier", for: indexPath) as! ContactCardCell
             cell.updateCell(withData: AddressBook.contacts[indexPath.row])
             return cell
           }
        ```

    + `UIApplication`:

        ```diff
        -  @IBAction func btnCurrentLocationTouchedUpInside(sender: UIButton) {
        -    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        +  @IBAction func btnCurrentLocationTouchedUpInside(_ sender: UIButton) {
        +    UIApplication.shared.isNetworkActivityIndicatorVisible = true
        ```

- Khởi tạo `NSNumber` bắt buộc phải có tham số `value`:

    ```diff
    - NSNumber(minFare)
    + NSNumber(value: minFare)
    ```

#2. Convention:

- Enum: Tên biến đổi từ viết hoa -> viết thường
- `UIView`: các hàm và thuộc tính trả về giá trị `Bool` sẽ có thêm `is` ở đầu:

    ```diff
    -  @IBAction func swtConvertDirectionValueChanged(sender: UISwitch) {
    -    _vResultLunarZodiacNames.hidden = !_swtConvertDirection.on
    +  @IBAction func swtConvertDirectionValueChanged(_ sender: UISwitch) {
    +    _vResultLunarZodiacNames.isHidden = !_swtConvertDirection.isOn
         convertDays()
         
    -    if _swtConvertDirection.on {
    +    if _swtConvertDirection.isOn {
    ```

- `UIViewController`: đổi tên `destinationViewController` thành `destination`

    ```diff
    -  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    -    let mapVC = segue.destinationViewController as! MapViewController
    +  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    +    let mapVC = segue.destination as! MapViewController
    ```

- `NSUserDefault` đổi tên thành `UserDefault`:

    ```diff
    -    let userDefaults = NSUserDefaults.standardUserDefaults()
    +    let userDefaults = UserDefaults.standard
    ```