//
//  ViewController.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import SwiftyJSON
import ESPullToRefresh
import KRProgressHUD
import Toaster
import SafariServices
import ReachabilitySwift

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  fileprivate var _postsData: [JSON] = []
  fileprivate var _currentPage: Int = 1
  fileprivate var _reachability: Reachability? = nil
  fileprivate var _noNetworkError: Bool = false
  fileprivate var _vPullToRefreshHeader: ESRefreshHeaderView? = nil;
  fileprivate var _vInfiniteScrolling: ESRefreshFooterView? = nil;
  
  @IBOutlet weak var _tblPosts: UITableView!
  @IBOutlet var _vNoNetworkError: UIView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupViews()
  }
  
  // UITableViewDataSource methods
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return _postsData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellIdentifier") as! PostCell
    
    let postData = _postsData[indexPath.row]
    cell.updateCell(withData: postData)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if _noNetworkError {
      Toast(text: "Không có kết nối mạng!\nKhông thể mở nội dung chi tiết", duration: 1).show()
      return
    }
    
    let postData = _postsData[indexPath.row]
    let viewUrl = "http://www.depvd.com/\(postData["viewUrl"].stringValue)"
    let safariVC = SFSafariViewController(url: URL(string: viewUrl)!)
    self.present(safariVC, animated: true, completion: nil)
  }
  
  // UITableViewDelegate methods
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let postData = _postsData[indexPath.row]
    let widgetHeight = postData["widgetHeight"].doubleValue
    return CGFloat(widgetHeight * 365 / 192 + 72)
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return _noNetworkError ? _vNoNetworkError.frame.size.height : 0
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 10
  }
  
  // Custom functions
  fileprivate func _setupViews() {
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    _vPullToRefreshHeader = _tblPosts.es_addPullToRefresh { [weak self] in
      self?._refreshPostsData()
    }
    
    ServiceHelper.sharedHelper.setupReachability { [weak self] (reachable) in
      self?._toggleNoNetworkError(showing: !reachable)
    }
  }
  
  fileprivate func _refreshPostsData() {
    // Reset current showing page
    _currentPage = 1
    
    // Only show HUD at the first time loading data
    if _postsData.count == 0 {
      KRProgressHUD.show(message: "Đang lấy dữ liệu...")
    }
    
    ServiceHelper.sharedHelper.getImagesList { [weak self] (posts, error) in
      self?._tblPosts.es_stopPullToRefresh(ignoreDate: true)
      KRProgressHUD.dismiss()
      
      if error != nil {
        return
      }
      
      self?._postsData = posts
      self?._tblPosts.reloadData()
      Toast(text: "Cập nhật dữ liệu cho \(posts.count) ảnh mới", duration: 1).show()
      self?._tblPosts.es_resetNoMoreData()
      self?._vInfiniteScrolling = self?._tblPosts.es_addInfiniteScrolling { self?._loadMoreData() }
    }
  }
  
  fileprivate func _loadMoreData() {
    _currentPage += 1
    
    ServiceHelper.sharedHelper.getImagesList(page: _currentPage) { [weak self] (posts, error) in
      self?._tblPosts.es_stopLoadingMore()
      
      if error != nil {
        return
      }
      
      self?._postsData.append(contentsOf: posts)
      self?._tblPosts.reloadData()
      
      if posts.count == 0 {
        self?._tblPosts.es_noticeNoMoreData()
      }
    }
  }
  
  fileprivate func _toggleNoNetworkError(showing show: Bool) {
    if _noNetworkError == show {
      return
    }
    
    _noNetworkError = show
    
    if _noNetworkError {
      _vNoNetworkError.alpha = 0
      _vNoNetworkError.frame = CGRect(origin: CGPoint(x: 0, y: 42), size: _vNoNetworkError.bounds.size)
      self.view.addSubview(_vNoNetworkError)
    }
    
    UIView.animate(
      withDuration: 0.5,
      delay: 0,
      options: UIViewAnimationOptions(),
      animations: { [weak self] in
        self?._vNoNetworkError.alpha = show ? 1 : 0
        var frame = self?._vNoNetworkError.frame
        frame?.origin.y = (self?._tblPosts.frame.origin.y)! - (show ? 0 : (frame?.size.height)!)
        self?._vNoNetworkError.frame = frame!
      },
      completion: { [weak self] (completed) in
        if completed {
          self?._tblPosts.reloadData()
          
          if show {
            self?._tblPosts.es_noticeNoMoreData()
          } else {
            self?._tblPosts.es_resetNoMoreData()
            self?._vNoNetworkError.removeFromSuperview()
            
            if self?._postsData.count == 0 {
              self?._tblPosts.es_startPullToRefresh()
            }
          }
        }
      })
  }
}
