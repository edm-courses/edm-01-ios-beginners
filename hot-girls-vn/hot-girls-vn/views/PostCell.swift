//
//  PostCell.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage

class PostCell: UITableViewCell {
  @IBOutlet weak var _imgPostImage: UIImageView!
  @IBOutlet weak var _lblTitle: UILabel!
  @IBOutlet weak var _lblLikes: UILabel!
  
  func updateCell(withData data: JSON) {
    _imgPostImage.image = nil
    _lblTitle.text = data["title"].stringValue
    _lblLikes.text = data["shortLikeCount"].stringValue
    
    self._loadImage(fromData: data)
  }
  
  fileprivate func _loadImage(fromData data: JSON) {
    let widgetImage = data["widgetImage"].stringValue
    let imagePath = widgetImage.replacingOccurrences(of: "_wi.jpg", with: "_no.jpg")
    let imageUrl = "http://photo.depvd.com/\(imagePath)"
    
    // Load image manually with delayed 3s
    // self._loadImageManually(imageUrl)
    self._loadImageWithAlamofire(imageUrl)
  }
  
  fileprivate func _loadImageManually(_ imageUrl: String) {
    ServiceHelper.sharedHelper.downloadImage(imageUrl) { [weak self] (image) in
      self?._imgPostImage.image = image
    }
  }
  
  fileprivate func _loadImageWithAlamofire(_ imageUrl: String) {
    _imgPostImage.image = nil
    let url = URL(string: imageUrl)
    _imgPostImage.af_setImage(withURL: url!, imageTransition: .crossDissolve(0.5))
  }
}
