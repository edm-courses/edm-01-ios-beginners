//
//  ServiceHelper.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ReachabilitySwift

typealias ReachabilityStatusChangedHandler = (Bool) -> ()

extension NSObject {
  class func performAfter(_ delay: Double, _ callback: @escaping () -> ()) {
    if delay <= 0 {
      callback()
      return
    }
    
    let delayTime = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: delayTime) { callback() }
  }
}

class ServiceHelper: NSObject {
  // Singleton declarations
  static var sharedHelper = ServiceHelper()
  fileprivate override init() {}
  
  // Instance properties
  fileprivate var _reachability: Reachability? = nil
  
  func getImagesList(page: Int = 1, completion handler: @escaping ([JSON], NSError?) -> ()) {
    let currentTimestamp = Int(Date().timeIntervalSince1970)
    
    let paramPages = [
      // Use current timestamp for first page
      ["lac": "\(currentTimestamp)000", "p.cp": "2"],
      
      // For second pages & so on, use sooner 6 hours timestamp
      ["lac": "\(currentTimestamp-60*60*6)000", "p.cp": "2"],
      ["lac": "\(currentTimestamp-60*60*12)000", "p.cp": "2"]
    ]
    
    // No more data
    if page > paramPages.count {
      handler([], nil)
      return
    }
    
    Alamofire.request("http://www.depvd.com/pajax/topic/list/vn", method: .get, parameters: paramPages[page-1])
      .responseData { (response) in
        switch response.result {
        case .success(let data):
          let jsonObj = JSON(data: data)
          let topics = jsonObj["topics"].arrayValue
          
          NSObject.performAfter(1) { handler(topics, nil) }
        case .failure(let error):
          handler([], error as NSError?)
        }
    }
  }
  
  func downloadImage(_ imageUrl: String, completion handler: @escaping (UIImage?) -> ()) {
    Alamofire.request(imageUrl, method: .get).responseImage { (response) in
      switch response.result {
      case .success(let image):
        NSObject.performAfter(3) { handler(image) }
      case .failure(let error):
        print(error)
        NSObject.performAfter(3) { handler(nil) }
      }
    }
  }
  
  func setupReachability(withHandler handler: @escaping ReachabilityStatusChangedHandler) {
    do {
      _reachability = Reachability()!;
      
      let statusHandler: ((Reachability) -> ()) = { handler($0.isReachable) }
      _reachability?.whenReachable = statusHandler
      _reachability?.whenUnreachable = statusHandler
      
      try _reachability!.startNotifier()
    } catch (let error as NSError) {
      print(error)
    }
  }
}
