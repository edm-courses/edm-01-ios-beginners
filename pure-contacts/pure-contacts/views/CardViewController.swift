//
//  SecondViewController.swift
//  pure-contacts
//
//  Created by Ethan Nguyen on 5/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class CardViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    for viewController in self.tabBarController!.viewControllers! {
      let navigationController = viewController as! UINavigationController
      print(navigationController)
      for subVC in navigationController.viewControllers {
        print("\t\(subVC)")
      }
    }
  }
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return AddressBook.contacts.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCardCellIdentifier", for: indexPath) as! ContactCardCell
    cell.updateCell(withData: AddressBook.contacts[indexPath.row])
    return cell
  }
}

