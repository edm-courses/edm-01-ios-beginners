//
//  FirstViewController.swift
//  pure-contacts
//
//  Created by Ethan Nguyen on 5/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet weak var _tblContacts: UITableView!
  fileprivate var _reusedCellAddresses = Set<String>()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]

    print("Load ListViewController")
    let tableHeight = _tblContacts.frame.size.height
    let reusableCells = ceil(tableHeight / 80.0)
    print("Table View height: \(tableHeight)")
    print("Reusable Cells: \(reusableCells)")
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return AddressBook.contacts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCellIdentifier") as! ContactListCell
    cell.updateCell(withData: AddressBook.contacts[indexPath.row])
    
    // Check how many reusable cells instantiated
    _reusedCellAddresses.insert(NSString(format: "%p", cell) as String)
    print("Reused cells count: \(_reusedCellAddresses.count)")
    
    return cell
  }
}
