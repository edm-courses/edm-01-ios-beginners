//
//  ContactCardCell.swift
//  pure-contacts
//
//  Created by Ethan Nguyen on 6/3/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class ContactCardCell: UICollectionViewCell {
  @IBOutlet weak var _imgAvatar: UIImageView!
  @IBOutlet weak var _lblName: UILabel!
  @IBOutlet weak var _lblPhone: UILabel!
  @IBOutlet weak var _lblEmail: UILabel!
  
  func updateCell(withData contactData: [String: String]) {
    var imageName = "img-avatar-placeholder-large"
    if let avatar = contactData["avatar"] {
      imageName = avatar + "-large"
    }
    _imgAvatar.image = UIImage(named: imageName)
    
    _lblName.text = "Name: " + contactData["name"]!
    _lblPhone.text = "Phone: " + contactData["phone"]!
    _lblEmail.text = "Email: " + contactData["email"]!
  }
}
