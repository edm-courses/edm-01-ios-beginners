//
//  SignInViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import KRProgressHUD
import JLToast

class SignInViewController: BaseViewController, GIDSignInUIDelegate {
  @IBOutlet weak var _navigationBar: UINavigationBar!
  @IBOutlet weak var _btnFacebook: UIButton!
  @IBOutlet weak var _btnGoogle: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    _navigationBar.titleTextAttributes = [
      NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    // Force sign out current user from everywhere
    MUser.signOutEverywhere()
    GIDSignIn.sharedInstance().uiDelegate = self
  }
  
  @IBAction func btnFacebookTouchedUpInside(sender: UIButton) {
    let loginManager = FBSDKLoginManager()
    
    loginManager.logInWithReadPermissions(["public_profile"], fromViewController: self) { (result, error) in
      if error != nil {
        JLToast.makeText(error.localizedDescription, duration: 1).show()
        return
      }
      
      let currentAccessToken = FBSDKAccessToken.currentAccessToken()
      if currentAccessToken == nil {
        return
      }
      
      let accessToken = currentAccessToken.tokenString
      let credential = FIRFacebookAuthProvider.credentialWithAccessToken(accessToken)
      
      KRProgressHUD.show()
      
      FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
        KRProgressHUD.dismiss()
        
        if error != nil {
          JLToast.makeText(error!.localizedDescription, duration: 1).show()
          return
        }
        
        MUser.syncFromCurrentUser()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.presentMainViewController()
      }
    }
  }
  
  @IBAction func btnGoogleTouchedUpInside(sender: UIButton) {
    KRProgressHUD.show()
    GIDSignIn.sharedInstance().signIn()
  }
  
  // GIDSignInUIDelegate
  func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
    KRProgressHUD.dismiss()
  }
}