//
//  SecondViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import Firebase
import JLToast
import AlamofireImage

class AccountViewController: BaseViewController {
  @IBOutlet weak var _imgProfilePhoto: UIImageView!
  @IBOutlet weak var _lblDisplayName: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if self.shouldRefreshViews {
      self._populateData()
      self.shouldRefreshViews = false
    }
  }
  
  // Button callbacks
  @IBAction func btnSignOutTouchedUpInside(sender: UIButton) {
    MUser.signOutEverywhere { (error) in
      if error != nil {
        JLToast.makeText(error!.localizedDescription, duration: 1).show()
      }
      
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      appDelegate.presentSignInViewController()
    }
  }
  
  // Private methods
  private func _populateData() {
    let currentUser = FIRAuth.auth()?.currentUser
    
    if currentUser == nil {
      return
    }
    
    _imgProfilePhoto.image = nil
    _imgProfilePhoto.af_setImageWithURL(currentUser!.photoURL!,
                                        filter: CircleFilter(),
                                        imageTransition: .CrossDissolve(0.5),
                                        runImageTransitionIfCached: true)
    _lblDisplayName.text = currentUser!.displayName
  }
}