//
//  MainTabBarController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/20/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
  override func viewDidLoad() {
    self.delegate = self
  }
  
  func requireAllTabsReloading() {
    self.selectedIndex = 0
    
    for subVC in self.viewControllers! {
      let navigation = subVC as! UINavigationController
      let viewController = navigation.viewControllers.first as! BaseViewController
      viewController.shouldRefreshViews = true
    }
  }
  
  func requireAllTabsPopingToRootVC() {
    for subVC in self.viewControllers! {
      let navigation = subVC as! UINavigationController
      navigation.popToRootViewControllerAnimated(false)
    }
  }
}
