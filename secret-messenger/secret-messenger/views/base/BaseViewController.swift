//
//  BaseViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/13/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import JLToast

class BaseViewController: UIViewController {
  var shouldRefreshViews: Bool = true
  
  override func viewDidLoad() {
    if self.navigationController != nil {
      self.navigationController?.navigationBar.titleTextAttributes = [
        NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 17)!
      ]
    }
    
    JLToastView.setDefaultValue(UIFont(name: "Roboto-Light", size: 17)!,
                                forAttributeName: JLToastViewFontAttributeName,
                                userInterfaceIdiom: .Phone)
  }
}
