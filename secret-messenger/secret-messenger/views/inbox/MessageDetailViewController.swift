//
//  MessageDetailViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/20/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import JLToast
import KRProgressHUD

@objc protocol MessageDetailDelegate: NSObjectProtocol {
  optional func messageDetailVC(messageDetailVC: MessageDetailViewController, didReplyWithMessage replyingMessage: MMessage)
}

class MessageDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
  @IBOutlet weak var _tblMessages: UITableView!
  @IBOutlet weak var _txtReplyAnchor: UITextField!
  @IBOutlet var _txtReply: UITextField!
  
  var delegate: MessageDetailDelegate?
  
  var conversationMessages: [MMessage] = [MMessage]()
  private var _topicMessage: MMessage? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    _topicMessage = self.conversationMessages.first
    if _topicMessage != nil {
      self.navigationItem.title = _topicMessage!.fromUserName
    }
    
    _txtReplyAnchor.inputAccessoryView = _txtReply
  }
  
  // UITableViewDataSource methods
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return conversationMessages.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let message = conversationMessages[indexPath.row]
    let position = message.fromCurrentUser() ? "Right" : "Left"
    
    let cell = tableView.dequeueReusableCellWithIdentifier("Conversation\(position)CellIdentifier") as! ConversationCell
    cell.updateCell(withData: message)
    
    return cell
  }
  
  // UITableViewDelegate methods
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    let message = conversationMessages[indexPath.row]
    let reuseID = message.fromCurrentUser() ? ConversationCell.ReuseIdentifiers.Right : ConversationCell.ReuseIdentifiers.Left
    
    let cell = tableView.dequeueReusableCellWithIdentifier(reuseID) as! ConversationCell
    return cell.heightForCell(withData: message)
  }
  
  // UITextFieldDelegate methods
  func textFieldDidBeginEditing(textField: UITextField) {
    if textField == _txtReplyAnchor {
      _txtReply.becomeFirstResponder()
    }
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField.text == nil || textField.text!.isEmpty {
      return false
    }
    
    textField.resignFirstResponder()
    self._replyMessage()
    return true
  }
  
  @IBAction func btnReplyPressed(sender: UIBarButtonItem) {
    _txtReplyAnchor.becomeFirstResponder()
  }
  
  private func _refreshWithMessage(message: MMessage) {
    _txtReply.text = nil
    self.conversationMessages.append(message)
    _tblMessages.reloadData()
    
    if self.delegate != nil &&
      self.delegate!.respondsToSelector(#selector(MessageDetailDelegate.messageDetailVC(_:didReplyWithMessage:))) {
      self.delegate!.messageDetailVC!(self, didReplyWithMessage: message)
    }
  }
  
  private func _replyMessage() {
    do {
      let message = try MMessage.composeMessage(withTitle: _topicMessage!.title,
                                                andContent: _txtReply.text,
                                                toUser: _topicMessage!.fromUserId,
                                                replyToMessage: _topicMessage!.mid)
      
      KRProgressHUD.show()
      try message.save { [weak self] (error, dbRef) in
        KRProgressHUD.dismiss()
        
        if error != nil {
          JLToast.makeText("Có lỗi xảy ra: \(error!.localizedDescription)", duration: 1).show()
        } else {
          JLToast.makeText("Gửi tin nhắn thành công!", duration: 1).show()
        }
        
        self?._refreshWithMessage(message)
      }
    } catch (let error as NSError) {
      KRProgressHUD.dismiss()
      JLToast.makeText("Có lỗi xảy ra: \(error.localizedDescription)", duration: 1).show()
    }
  }
}
