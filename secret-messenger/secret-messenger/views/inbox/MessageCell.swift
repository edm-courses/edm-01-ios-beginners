//
//  MessageCell.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/20/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage

class MessageCell: UITableViewCell {
  @IBOutlet weak var _imgProfilePhoto: UIImageView!
  @IBOutlet weak var _lblDisplayName: UILabel!
  @IBOutlet weak var _lblTitle: UILabel!
  @IBOutlet weak var _lblMessage: UILabel!
  
  func updateCell(withData message: MMessage) {
    _imgProfilePhoto.image = nil
    _lblDisplayName.text = message.fromUserName
    
    MUser.find(byId: message.fromUserId!) { [weak self] (result, error) in
      if result == nil || error != nil {
        return
      }
      
      let sender = result as! MUser
      self?._imgProfilePhoto.af_setImageWithURL(sender.photoURL!,
                                                filter: RoundedCornersFilter(radius: 10),
                                                imageTransition: .CrossDissolve(0.5))
      
      if let displayNameLabel = self?._lblDisplayName {
        displayNameLabel.text = sender.displayName
      }
    }
    
    _lblTitle.text = message.title
    
    _lblMessage.text = nil
    do { _lblMessage.text = try message.plainMessage() } catch {}
  }
}