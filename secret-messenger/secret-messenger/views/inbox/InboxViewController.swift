//
//  FirstViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import Firebase
import DGElasticPullToRefresh_CanStartLoading

class InboxViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, MessageDetailDelegate {
  @IBOutlet weak var _tblMessages: UITableView!
  
  private var _inboxMessages: [MMessage] = [MMessage]()
  private var _conversationMessages: [String: [MMessage]] = [String: [MMessage]]()
  private var _selectedMessageIndex: Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupTableView()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if self.shouldRefreshViews {
      _inboxMessages.removeAll()
      _tblMessages.reloadData()
      _tblMessages.contentInset = UIEdgeInsetsZero
      _tblMessages.dg_startLoading()
      self.shouldRefreshViews = false
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let messageDetailVC = segue.destinationViewController as! MessageDetailViewController
    messageDetailVC.delegate = self
    
    let selectedMessage = _inboxMessages[_selectedMessageIndex]
    
    if let conversationMessages = _conversationMessages[selectedMessage.mid!] {
      messageDetailVC.conversationMessages = conversationMessages
    }
  }
  
  // UITableViewDataSource methods
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return _inboxMessages.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("MessageCellIdentifier") as! MessageCell
    cell.updateCell(withData: _inboxMessages[indexPath.row])
    return cell
  }
  
  // UITableViewDelegate methods
  func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
    _selectedMessageIndex = indexPath.row
    return indexPath
  }
  
  // MessageDetailDelegate methods
  func messageDetailVC(messageDetailVC: MessageDetailViewController, didReplyWithMessage replyingMessage: MMessage) {
    _conversationMessages[replyingMessage.replyingMessageId!]?.append(replyingMessage)
    _tblMessages.reloadData()
  }
  
  private func _setupTableView() {
    var frame = self.view.bounds
    frame.origin.y = 64
    frame.size.height -= 64
    _tblMessages.frame = frame
    self.view.addSubview(_tblMessages)
    
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
    _tblMessages.dg_addPullToRefreshWithActionHandler({ [weak self] in self?._refreshData() }, loadingView: loadingView)
    _tblMessages.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
    _tblMessages.dg_setPullToRefreshBackgroundColor(_tblMessages.backgroundColor!)
    _tblMessages.dg_startLoading()
  }
  
  func _refreshData() {
    MMessage.allInboxMessagesForCurrentUser { [weak self] (inboxMessages, conversationMessages, error) in
      if error != nil {
        print(error!.localizedDescription)
      }
      
      self?._inboxMessages.removeAll()
      self?._inboxMessages.appendContentsOf(inboxMessages)
      self?._conversationMessages = conversationMessages
      
      self?._tblMessages.reloadData()
      self?._tblMessages.dg_stopLoading()
    }
  }
}