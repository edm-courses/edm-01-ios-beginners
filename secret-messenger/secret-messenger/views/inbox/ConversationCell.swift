//
//  ConversationCell.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/22/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage

extension String {
  func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: CGFloat.max)
    
    let boundingBox = self.boundingRectWithSize(constraintRect,
                                                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                attributes: [NSFontAttributeName: font],
                                                context: nil)
    
    return boundingBox.height
  }
}

class ConversationCell: UITableViewCell {
  internal struct ReuseIdentifiers {
    static let Left: String = "ConversationLeftCellIdentifier"
    static let Right: String = "ConversationRightCellIdentifier"
  }
  
  @IBOutlet weak var _imgProfilePhoto: UIImageView!
  @IBOutlet weak var _lblMessage: UILabel!
  
  func updateCell(withData message: MMessage) {
    _imgProfilePhoto.image = nil
    MUser.find(byId: message.fromUserId!) { [weak self] (result, error) in
      if result == nil || error != nil {
        return
      }
      
      let sender = result as! MUser
      self?._imgProfilePhoto.af_setImageWithURL(sender.photoURL!,
                                                filter: CircleFilter(),
                                                imageTransition: .CrossDissolve(0.5))
    }
    
    _lblMessage.text = nil
    do { try _lblMessage.text = message.plainMessage(shouldBeTruncated: false) } catch {}
  }
  
  func heightForCell(withData message: MMessage) -> CGFloat {
    let minHeight = CGFloat(60)
    let labelWidth = _lblMessage.frame.size.width
    let labelFont = _lblMessage.font
    let messagePadding = CGFloat(20)
    
    do {
      let plainMessage = try message.plainMessage(shouldBeTruncated: false)
      let height = plainMessage.heightWithConstrainedWidth(labelWidth, font: labelFont)
      return max(height + messagePadding*2, minHeight)
    } catch {
      return 0
    }
  }
}