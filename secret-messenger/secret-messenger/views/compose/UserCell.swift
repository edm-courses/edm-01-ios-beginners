//
//  UserCell.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/22/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage

class UserCell: UITableViewCell {
  @IBOutlet weak var _imgProfilePicture: UIImageView!
  @IBOutlet weak var _lblDisplayName: UILabel!
  
  func updateCell(withData userData: MUser) {
    if let photoURL = userData.photoURL {
      _imgProfilePicture.af_setImageWithURL(photoURL, filter: CircleFilter())
    }
    
    _lblDisplayName.text = userData.displayName
  }
}