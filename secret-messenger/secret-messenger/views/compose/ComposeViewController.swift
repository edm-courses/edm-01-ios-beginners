//
//  ComposeViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/13/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage
import JLToast
import KRProgressHUD

class ComposeViewController: BaseViewController, ListUsersDelegate, UITextFieldDelegate {
  struct ValidationErrors {
    static var EmptyReceiverError = "Thiếu người nhận!"
    static var EmptyTitleError = "Thiếu tiêu đề!"
    static var EmptyMessageError = "Thiếu nội dung tin nhắn!"
  }
  
  @IBOutlet weak var _imgReceiverPhoto: UIImageView!
  @IBOutlet weak var _lblReceiverName: UILabel!
  @IBOutlet weak var _txtTitle: UITextField!
  @IBOutlet weak var _txtMessage: UITextView!
  
  private var _receiver: MUser? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let tapGesture = UITapGestureRecognizer(target: _txtMessage, action: #selector(resignFirstResponder))
    self.view.addGestureRecognizer(tapGesture)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let listUsersVC = segue.destinationViewController as! ListUsersViewController
    listUsersVC.delegate = self
  }
  
  // ListUsersDelegate methods
  func listUserViewController(listUserVC: ListUsersViewController, didSelectUser selectedUser: MUser) {
    _receiver = selectedUser
    
    if let photoURL = selectedUser.photoURL {
      _imgReceiverPhoto.af_setImageWithURL(photoURL, filter: CircleFilter())
    }
    _lblReceiverName.text = selectedUser.displayName
  }
  
  // UITextFieldDelegate
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField.text == nil || textField.text!.isEmpty {
      JLToast.makeText(ValidationErrors.EmptyTitleError, duration: 1).show()
    } else {
      _txtMessage.becomeFirstResponder()
    }
    
    return false
  }
  
  @IBAction func btnSendPressed(sender: UIButton) {
    self._checkAndDeliverMessage()
  }
  
  private func _clearAllReceipts() {
    _receiver = nil
    _imgReceiverPhoto.image = nil
    _lblReceiverName.text = nil
    _txtTitle.text = nil
    _txtMessage.text = nil
    
    _txtTitle.resignFirstResponder()
    _txtMessage.resignFirstResponder()
  }
  
  private func _validateReceipts() -> String? {
    if _receiver == nil {
      return ValidationErrors.EmptyReceiverError
    }
    
    if _txtTitle.text == nil || _txtTitle.text!.isEmpty {
      return ValidationErrors.EmptyTitleError
    }
    
    if _txtMessage.text.isEmpty {
      return ValidationErrors.EmptyMessageError
    }
    
    return nil
  }
  
  private func _checkAndDeliverMessage() {
    if let validationError = self._validateReceipts() {
      JLToast.makeText(validationError, duration: 1).show()
      return
    }
    
    _txtTitle.resignFirstResponder()
    _txtMessage.resignFirstResponder()
    
    do {
      let message = try MMessage.composeMessage(withTitle: _txtTitle.text,
                                                andContent: _txtMessage.text,
                                                toUser: _receiver!.uid)
      
      KRProgressHUD.show()
      try message.save { [weak self] (error, dbRef) in
        KRProgressHUD.dismiss()
        
        if error != nil {
          JLToast.makeText("Có lỗi xảy ra: \(error!.localizedDescription)", duration: 1).show()
        } else {
          JLToast.makeText("Gửi tin nhắn thành công!", duration: 1).show()
          self?._clearAllReceipts()
        }
      }
    } catch (let error as NSError) {
      KRProgressHUD.dismiss()
      JLToast.makeText("Có lỗi xảy ra: \(error.localizedDescription)", duration: 1).show()
    }
  }
}
