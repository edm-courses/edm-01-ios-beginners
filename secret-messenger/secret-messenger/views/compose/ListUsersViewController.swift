//
//  ListUsersViewController.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/17/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import DGElasticPullToRefresh_CanStartLoading

@objc protocol ListUsersDelegate: NSObjectProtocol {
  optional func listUserViewController(listUserVC: ListUsersViewController, didSelectUser selectedUser: MUser)
}

class ListUsersViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet weak var _tblUsers: UITableView!
  
  var delegate: ListUsersDelegate?
  
  private var _usersData: [MUser] = [MUser]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupTableView()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    _tblUsers.dg_removePullToRefresh()
  }
  
  // UITableViewDataSource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return _usersData.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("UserCellIdentifier") as! UserCell
    cell.updateCell(withData: _usersData[indexPath.row])
    return cell
  }
  
  // UITableViewDelegate
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.navigationController?.popViewControllerAnimated(true)
    
    if self.delegate != nil &&
      self.delegate!.respondsToSelector(#selector(ListUsersDelegate.listUserViewController(_:didSelectUser:))) {
      self.delegate?.listUserViewController!(self, didSelectUser: _usersData[indexPath.row])
    }
  }
  
  private func _setupTableView() {
    var frame = self.view.bounds
    frame.origin.y = 64
    frame.size.height -= 64
    _tblUsers.frame = frame
    self.view.addSubview(_tblUsers)
    
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
    _tblUsers.dg_addPullToRefreshWithActionHandler({ [weak self] in self?._refreshData() }, loadingView: loadingView)
    _tblUsers.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
    _tblUsers.dg_setPullToRefreshBackgroundColor(_tblUsers.backgroundColor!)
    _tblUsers.dg_startLoading()
  }
  
  private func _refreshData() {
    MUser.allUsersExceptCurrent { [weak self] (results, error) in
      self?._tblUsers.dg_stopLoading()
      
      if error != nil {
        return
      }
      
      let users = results as! [MUser]
      self?._usersData.removeAll()
      self?._usersData.appendContentsOf(users)
      self?._tblUsers.reloadData()
    }
  }
}
