//
//  AppDelegate.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import JLToast
import KRProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
  var window: UIWindow?
  
  private var _mainVC: MainTabBarController!
  private var _signInVC: SignInViewController!

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    self._configLibraries(inApplication: application, withOptions: launchOptions)
    self._checkSignedInUserToShowRootView()
    return true
  }

  func applicationWillResignActive(application: UIApplication) {
  }

  func applicationDidEnterBackground(application: UIApplication) {
  }

  func applicationWillEnterForeground(application: UIApplication) {
  }

  func applicationDidBecomeActive(application: UIApplication) {
    FBSDKAppEvents.activateApp()
  }

  func applicationWillTerminate(application: UIApplication) {
  }
  
  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    var handleResult = false
    
    print(url.scheme)
    
    if url.scheme == "fb\(FBSDKSettings.appID())" {
      handleResult = FBSDKApplicationDelegate.sharedInstance().application(application,
                                                                           openURL: url,
                                                                           sourceApplication: sourceApplication,
                                                                           annotation: annotation)
    } else {
      handleResult = GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    return handleResult
  }
  
  // GIDSignInDelegate methods
  func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
    if error != nil {
      JLToast.makeText(error.localizedDescription, duration: 1).show()
      return
    }
    
    let authentication = user.authentication
    let credential = FIRGoogleAuthProvider.credentialWithIDToken(authentication.idToken,
                                                                 accessToken: authentication.accessToken)

    KRProgressHUD.show()
    
    FIRAuth.auth()?.signInWithCredential(credential) { [weak self] (user, error) in
      KRProgressHUD.dismiss()
      
      if error != nil {
        JLToast.makeText(error!.localizedDescription, duration: 1).show()
        return
      }
      
      MUser.syncFromCurrentUser()
      self?.presentMainViewController()
    }
  }
  
  func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!, withError error: NSError!) {
    if error != nil {
      JLToast.makeText(error.localizedDescription, duration: 1).show()
      return
    }
    
    self.presentSignInViewController()
  }
  
  // Custom functions
  func presentMainViewController() {
    _mainVC.requireAllTabsReloading()
    
    let rootVC = self.window!.rootViewController
    
    if rootVC == _mainVC {
      _signInVC.dismissViewControllerAnimated(true, completion: nil)
    } else if rootVC == _signInVC {
      _signInVC.presentViewController(_mainVC, animated: true, completion: nil)
    }
  }
  
  func presentSignInViewController() {
    _mainVC.requireAllTabsPopingToRootVC()
    
    let rootVC = self.window!.rootViewController
    
    if rootVC == _mainVC {
      _mainVC.presentViewController(_signInVC, animated: true, completion: nil)
    } else if rootVC == _signInVC {
      _mainVC.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  // Private functions
  private func _configLibraries(inApplication application: UIApplication, withOptions launchOptions: [NSObject: AnyObject]?) {
    // Firebase configs
    FIRApp.configure()
    FIRDatabase.database().reference().keepSynced(true)
    
    // Facebook configs
    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    // Google configs
    GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
    GIDSignIn.sharedInstance().delegate = self
    
    JLToastView.setDefaultValue(UIFont(name: "Roboto-Medium", size: 17)!,
                                forAttributeName: JLToastViewFontAttributeName,
                                userInterfaceIdiom: .Phone)
  }
  
  // Check if user is signed in & stored credentials locally
  private func _checkSignedInUserToShowRootView() {
    let userSignedIn = FIRAuth.auth()?.currentUser != nil
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    _mainVC = storyboard.instantiateViewControllerWithIdentifier("MainVCIdentifier") as! MainTabBarController
    _mainVC.modalTransitionStyle = .FlipHorizontal
    _signInVC = storyboard.instantiateViewControllerWithIdentifier("SignInVCIdentifier") as! SignInViewController
    _signInVC.modalTransitionStyle = .FlipHorizontal
    
    self.window!.rootViewController = userSignedIn ? _mainVC : _signInVC
  }
}