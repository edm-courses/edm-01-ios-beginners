//
//  MMessage.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper
import CryptoSwift

class MMessage: MBase {
  // Identifiers
  var mid: String?                  // Message ID
  var fromUserId: String?           // Sender ID (MUser)
  var fromUserName: String?         // Sender Name (MUser)
  var toUserId: String?             // Receiver ID (MUser)
  var replyingMessageId: String?    // ID of message this one replied to (MMessage)
  
  // Content
  var title: String?
  var encryptedMessage: String? = ""
  
  // Timestamps
  var sendTime: NSDate? = NSDate()
  var openTime: NSDate?
  
  // Statuses
  var opened: Bool = false
  var archived: Bool = false
  
  override init() { super.init() }
  
  required init?(_ map: Map) {
    super.init(map)

    let attributes = ["mid", "fromUserId", "toUserId", "title", "encryptedMessage", "sendTime"]
    
    // All attributes must be present
    let validations = attributes.map { map[$0].value() !== nil && map[$0].value() !== "" }.reduce(true) { $0 && $1 }
    if !validations {
      return nil
    }
    
    // Equivalent to
//    if map["mid"].value() === nil || map["mid"].value() == "" ||
//      map["fromUserId"].value() === nil || map["fromUserId"].value() == "" ||
//      map["toUserId"].value() === nil || map["toUserId"].value() == "" ||
//      map["title"].value() === nil || map["title"].value() == "" ||
//      map["encryptedMessage"].value() === nil || map["encryptedMessage"].value() == "" ||
//      map["sendTime"].value() === nil || map["sendTime"].value() == "" {
//      return nil
//    }
  }
  
  override func mapping(map: Map) {
    mid               <- map["mid"]
    fromUserId        <- map["fromUserId"]
    fromUserName      <- map["fromUserName"]
    toUserId          <- map["toUserId"]
    replyingMessageId <- map["replyingMessageId"]
    
    title             <- map["title"]
    encryptedMessage  <- map["encryptedMessage"]
    
    let dateTransform = DateTransform()
    sendTime          <- (map["sendTime"], dateTransform)
    openTime          <- (map["openTime"], dateTransform)
    
    opened            <- map["opened"]
    archived          <- map["archived"]
  }
  
  override var description: String {
    return "<MMesage Id: \(self.mid ?? "N/A") - Title: \(self.title) - Encrypted message: \(self.encryptedMessage) - Send time: \(self.sendTime)>"
  }
  
  override func validate() -> (ModelValidationError, String?) {
    if self.fromUserId == nil || self.fromUserId == "" ||
      self.toUserId == nil || self.toUserId == "" {
      return (.InvalidId, "mid, fromUserId, toUserId")
    }
    
    if self.title == nil || self.title == "" ||
      self.encryptedMessage == nil || self.encryptedMessage == "" {
      return (.InvalidBlankAttribute, "title, encryptedMessage")
    }
    
    if self.sendTime == nil {
      return (.InvalidTimestamp, "sendTime")
    }
    
    // Valid
    return (.Valid, nil)
  }
  
  override class func collectionName() -> String {
    return "messages"
  }
  
  override class func primaryKey() -> String {
    return "mid"
  }
  
  override class func mapObject(jsonObject: NSDictionary) -> MBase? {
    return Mapper<MMessage>().map(jsonObject)
  }
  
  // Class methods
  class func composeMessage(withTitle title: String?, andContent content: String?, toUser toUserId: String?, replyToMessage replyingMessageId: String? = nil) throws -> MMessage {
    let message = MMessage()
    
    message.title = title
    message.encryptedMessage = try message._encryptedMessage(fromPlain: content)
    
    let currentUser = FIRAuth.auth()!.currentUser!
    message.fromUserId = currentUser.uid
    message.fromUserName = currentUser.displayName
    message.toUserId = toUserId
    
    if replyingMessageId != nil {
      message.replyingMessageId = replyingMessageId
    }
    
    return message
  }
  
  class func allInboxMessagesForCurrentUser(completion handler: InboxMessageHandler) {
    let currentUser = FIRAuth.auth()?.currentUser
    
    let queryClause: [QueryClausesEnum: AnyObject] = [
      .OrderedChildKey: currentUser!.uid,
      .ExactValue: true
    ]
    MTopic.query(withClause: queryClause) { (results, error) in
      if error != nil {
        handler(inboxMessages: [], conversationMessages: [:], error: error)
        return
      }
      
      let topics = results as! [MTopic]
      let inboxMessageIds = topics.filter { $0.topicMessageId != nil }.map { $0.topicMessageId! }
      
      if inboxMessageIds.isEmpty {
        handler(inboxMessages: [], conversationMessages: [:], error: NotFoundError)
        return
      }
      
      let uniqueMessageIdsSet = Set<String>(inboxMessageIds)
      let uniqueMessageIds = [String](uniqueMessageIdsSet).sort()
      self.retrieveTopicMessages(fromIds: uniqueMessageIds, completion: handler)
    }
  }
  
  class func retrieveTopicMessages(fromIds messageIds: [String], completion handler: InboxMessageHandler) {
    let queryClause: [QueryClausesEnum: AnyObject] = [
      .OrderedChildKey: "mid",
      .StartValue: messageIds.first!,
      .EndValue: messageIds.last!
    ]
    
    self.query(withClause: queryClause) { (messages, error) in
      if error != nil {
        handler(inboxMessages: [], conversationMessages: [:], error: error)
        return
      }
      
      // Filter to get inbox messages only
      let inboxMessages = (messages as! [MMessage]).filter { messageIds.contains($0.mid!) }
        .sort { $0.sendTime?.compare($1.sendTime!) == .OrderedDescending }
      var conversationMessages = [String: [MMessage]]()
      
      self.retrieveRepliedMessages(forMessageIds: messageIds) { (replyingMessages, error) in
        for message in inboxMessages {
          conversationMessages[message.mid!] = message.filterRepliedMessages(fromMessages: replyingMessages as! [MMessage])
        }
        
        handler(inboxMessages: inboxMessages, conversationMessages: conversationMessages, error: nil)
      }
    }
  }
  
  class func retrieveRepliedMessages(forMessageIds messageIds: [String], completion handler: CollectionQueryResultHandler) {
    let queryClause: [QueryClausesEnum: AnyObject] = [
      .OrderedChildKey: "replyingMessageId",
      .StartValue: messageIds.first!,
      .EndValue: messageIds.last!
    ]
    
    self.query(withClause: queryClause, completion: handler)
  }
  
  override func save(completion handler: UpdateValueHandler? = nil) throws -> String {
    let messageId = try super.save(completion: handler)
    
    var topicData = [String: AnyObject]()
    topicData[self.fromUserId!] = true
    topicData[self.toUserId!] = true
    
    if self.replyingMessageId != nil {
      topicData["topicMessageId"] = self.replyingMessageId!
    } else {
      // Not replying any message, topicMessageId is messageId itself
      topicData["topicMessageId"] = messageId
    }
    
    let updatesManifest = ["/message_topics/\(messageId)": topicData]
    let dbRef = FIRDatabase.database().reference()
    dbRef.updateChildValues(updatesManifest)
    
    return messageId
  }
  
  func filterRepliedMessages(fromMessages messages: [MMessage]) -> [MMessage] {
    var allMessages = [self]
    let replyingMessages = messages.filter { $0.replyingMessageId == self.mid }.sort { $0.sendTime?.compare($1.sendTime!) == .OrderedAscending }
    allMessages.appendContentsOf(replyingMessages)
    
    return allMessages
  }
  
  // Instance methods
  func markAsRead() throws {
    self.openTime = NSDate()
    self.opened = true
    try self.save()
  }
  
  func archive() throws {
    self.archived = true
    try self.save()
  }
  
  func unArchive() throws {
    self.archived = false
    try self.save()
  }
  
  func fromCurrentUser() -> Bool {
    let currentUser = FIRAuth.auth()?.currentUser
    return self.fromUserId == currentUser?.uid
  }
  
  func plainMessage(shouldBeTruncated truncated: Bool = true) throws -> String {
    let decryptedMessage = try self._decryptedMessage()
    
    if truncated {
      var truncatedPosition = decryptedMessage.characters.count / 5
      
      if truncatedPosition <= 5 {
        truncatedPosition = 5
      }
      
      if truncatedPosition > decryptedMessage.characters.count {
        truncatedPosition = decryptedMessage.characters.count
      }
      
      let truncatedIndex = decryptedMessage.startIndex.advancedBy(truncatedPosition)
      return decryptedMessage.substringToIndex(truncatedIndex) + "..."
    }
    
    return decryptedMessage
  }
  
  private func _decryptedMessage() throws -> String {
    let cipher = try AES(key: CConstants.kAesKey, iv: CConstants.kAesIv)
    let encryptedContent = self.encryptedMessage ?? ""
    let decryptedMessage = try encryptedContent.decryptBase64ToString(cipher)
    return String(decryptedMessage)
  }
  
  private func _encryptedMessage(fromPlain plainMessage: String?) throws -> String {
    let cipher = try AES(key: CConstants.kAesKey, iv: CConstants.kAesIv)
    let plainData = NSString(string: plainMessage ?? "").dataUsingEncoding(NSUTF8StringEncoding)!
    let encryptedData = try plainData.encrypt(cipher)
    return encryptedData.arrayOfBytes().toBase64()!
  }
}