//
//  FirebaseDbRef.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Firebase

struct CConstants {
  static let kAesKey = "vn.ethan605.ios.secret-messenger" // App Bundle ID
  static let kAesIv = "1743719862541676"                  // Facebook App ID
}

typealias ObjectQueryResultHandler = (result: MBase?, error: NSError?) -> ()
typealias CollectionQueryResultHandler = (results: [MBase], error: NSError?) -> ()
typealias StatusReturnHandler = (status: Bool, error: NSError?) -> ()
typealias ErrorHandler = (NSError?) -> ()
typealias UpdateValueHandler = (NSError?, FIRDatabaseReference) -> ()
typealias InboxMessageHandler = (inboxMessages: [MMessage], conversationMessages: [String: [MMessage]], error: NSError?) -> ()

let NotFoundError: NSError = {
  return NSError(domain: "Not found", code: -1002, userInfo: [NSLocalizedDescriptionKey: "Result(s) not found"])
}()

enum QueryClausesEnum: String {
  case OrderedChildKey = "OrderedChildKey"
  case StartValue = "StartValue"
  case EndValue = "EndValue"
  case ExactValue = "ExactValue"
}

enum ModelValidationError: ErrorType {
  case Valid
  case InvalidId
  case InvalidTimestamp
  case InvalidBlankAttribute
}