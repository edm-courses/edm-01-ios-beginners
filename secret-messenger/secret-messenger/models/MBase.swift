//
//  MBase.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/12/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper
//import SwiftyJSON

class MBase: NSObject, Mappable {
  class func collectionName() -> String {
    fatalError("This method must be overridden")
  }
  
  class func primaryKey() -> String {
    fatalError("This method must be overridden")
  }
  
  class func mapObject(jsonObject: NSDictionary) -> MBase? {
    fatalError("This method must be overridden")
  }
  
  override init() {}
  
  required init?(_ map: Map) {
  }
  
  func mapping(map: Map) {
    fatalError("This method must be overridden")
  }
  
  func validate() -> (ModelValidationError, String?) {
    fatalError("This method must be overridden")
  }
  
  // Read
  class func find(byId objectId: String, completion handler: ObjectQueryResultHandler) {
    let dbRef = FIRDatabase.database().reference()
    let collectionName = self.collectionName()
    
    let query = dbRef.child("\(collectionName)/\(objectId)")
    
    query.observeEventType(.Value, withBlock: { (snapshot) in
      if !(snapshot.value is NSDictionary) {
        handler(result: nil, error: NotFoundError)
        return
      }
      
      let resultDict = snapshot.value as! NSDictionary
      let resultModel = self.mapObject(resultDict)
      handler(result: resultModel, error: nil)
    }) { (error) in
      handler(result: nil, error: error)
    }
  }
  
  class func query(withClause queryClause: [QueryClausesEnum: AnyObject]? = nil,
                   completion handler: CollectionQueryResultHandler) {
    let dbRef = FIRDatabase.database().reference()
    let collectionName = self.collectionName()
    
    var query = dbRef.child(collectionName) as FIRDatabaseQuery
    
    if queryClause != nil {
      if let orderedChildKey = queryClause![.OrderedChildKey] {
        query = query.queryOrderedByChild(orderedChildKey as! String)
      }
      
      if let startValue = queryClause![.StartValue] {
        query = query.queryStartingAtValue(startValue)
      }
      
      if let endValue = queryClause![.EndValue] {
        query = query.queryEndingAtValue(endValue)
      }
      
      if let exactValue = queryClause![.ExactValue] {
        query = query.queryEqualToValue(exactValue)
      }
    }
    
    query.observeSingleEventOfType(.Value, withBlock: { snapshot in
      if !(snapshot.value is NSDictionary) {
        handler(results: [], error: NotFoundError)
        return
      }
      
      var resultModels: [MBase] = []
      let results = snapshot.value as! [String: NSDictionary]
      
      for (_, resultDict) in results {
        let resultModel = self.mapObject(resultDict)
        
        if resultModel != nil {
          resultModels.append(resultModel!)
        }
      }
      
      handler(results: resultModels, error: nil)
    }) { (error) in
      handler(results: [], error: error)
    }
  }
  
  // Write
  func save(completion handler: UpdateValueHandler? = nil) throws -> String {
    let (validation, errors) = self.validate()
    let errorDomain = "Model validation failed"
    
    switch validation {
    case .Valid: break
    case .InvalidId:
      throw NSError(domain: errorDomain, code: -1, userInfo: [NSLocalizedDescriptionKey: "IDs \(errors!) must not be blank"])
    case .InvalidTimestamp:
      throw NSError(domain: errorDomain, code: -2, userInfo: [NSLocalizedDescriptionKey: "Timestamps \(errors!) must not be blank"])
    case .InvalidBlankAttribute:
      throw NSError(domain: errorDomain, code: -3, userInfo: [NSLocalizedDescriptionKey: "Attributes \(errors!) must not be blank"])
    }
    
    let dbRef = FIRDatabase.database().reference()
    let collectionName = self.dynamicType.collectionName()
    let primaryKey = self.dynamicType.primaryKey()
    
    var objectId = ""
    
    if let primaryKeyValue = self.valueForKey(primaryKey) {
      objectId = self._saveAsUpdate(inDb: dbRef, inCollection: collectionName, withId: primaryKeyValue as! String, completion: handler)
    } else {
      objectId = self._saveAsNew(inDb: dbRef, inCollection: collectionName, withPrimaryKey: primaryKey, completion: handler)
    }
    
    return objectId
  }
  
  func _saveAsUpdate(inDb dbRef: FIRDatabaseReference, inCollection colName: String, withId objectId: String, completion handler: UpdateValueHandler?) -> String {
    let objectJson = self.toJSON()
    let updatesManifest = ["/\(colName)/\(objectId)": objectJson]
    
    if handler != nil {
      dbRef.updateChildValues(updatesManifest, withCompletionBlock: handler!)
    } else {
      dbRef.updateChildValues(updatesManifest)
    }
    
    return objectId
  }
  
  private func _saveAsNew(inDb dbRef: FIRDatabaseReference, inCollection colName: String, withPrimaryKey primaryKey: String, completion handler: UpdateValueHandler?) -> String {
    let objectId = dbRef.child(colName).childByAutoId().key
    
    var objectJson = self.toJSON()
    objectJson[primaryKey] = objectId
    
    let updatesManifest = ["/\(colName)/\(objectId)": objectJson]
    
    if handler != nil {
      dbRef.updateChildValues(updatesManifest, withCompletionBlock: handler!)
    } else {
      dbRef.updateChildValues(updatesManifest)
    }
    
    return objectId
  }
}