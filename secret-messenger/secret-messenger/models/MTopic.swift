//
//  MTopic.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/23/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper

class MTopic: MBase {
  // Use message_topics collection as a middle mapping:
  // Any user who involve to this topic can retrieve messages
  /*
   "message_id": {
   "user_1_id": true,
   "user_2_id": true,
   topicMessageId: "topic_message_id"
   }
   */
  var topicMessageId: String?
  
  override init() { super.init() }
  
  required init?(_ map: Map) {
    super.init(map)
  }
  
  override func mapping(map: Map) {
    topicMessageId <- map["topicMessageId"]
  }
  
  override class func collectionName() -> String {
    return "message_topics"
  }
  
  override class func primaryKey() -> String {
    return "topicMessageId"
  }
  
  override class func mapObject(jsonObject: NSDictionary) -> MBase? {
    return Mapper<MTopic>().map(jsonObject)
  }

  override var description: String {
    return "<MTopic Id: \(self.topicMessageId ?? "N/A")>"
  }
}
