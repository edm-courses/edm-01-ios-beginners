//
//  MUser.swift
//  secret-messenger
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper
import FBSDKLoginKit
import GoogleSignIn

class MUser: MBase {
  var uid: String?
  var providerID: String?
  var displayName: String?
  var email: String?
  var photoURL: NSURL?
  
  override class func collectionName() -> String {
    return "synced_users"
  }
  
  override class func primaryKey() -> String {
    return "uid"
  }
  
  override class func mapObject(jsonObject: NSDictionary) -> MBase? {
    return Mapper<MUser>().map(jsonObject)
  }
  
  class func syncFromCurrentUser() {
    let currentUser = FIRAuth.auth()?.currentUser
    print(currentUser?.displayName)
    
    if currentUser == nil {
      return
    }
    
    let syncedUser = MUser()
    let attributes = ["uid", "displayName", "email", "photoURL"]
    
    for attribute in attributes {
      let value = currentUser?.valueForKey(attribute)
      
      if value != nil {
        syncedUser.setValue(value, forKey: attribute)
      }
    }
    
    if let provider = currentUser?.providerData.first {
      syncedUser.providerID = provider.providerID
    }
    
    do {
      try syncedUser.save()
    } catch (let error as NSError) {
      print(error.localizedDescription)
    }
  }
  
  class func signOutEverywhere(completion handler: ((NSError?) -> ())? = nil) {
    do {
      try FIRAuth.auth()?.signOut()
      FBSDKLoginManager().logOut()
      GIDSignIn.sharedInstance().signOut()
      
      if handler != nil { handler!(nil) }
    } catch (let error as NSError) {
      if handler != nil { handler!(error) }
    }
  }
  
  override init() { super.init() }
  
  required init?(_ map: Map) {
    super.init(map)
    
    let attributes = ["uid", "providerID", "displayName"]
    let validations = attributes.map { map[$0] !== nil }.reduce(true) { $0 && $1 }
    
    if !validations {
      return nil
    }
  }
  
  override func mapping(map: Map) {
    uid         <- map["uid"]
    providerID  <- map["providerID"]
    displayName <- map["displayName"]
    email       <- map["email"]
    photoURL    <- (map["photoURL"], URLTransform())
  }
  
  override var description: String {
    return "<MUser Id: \(self.uid!) - Display name: \(self.displayName!)>"
  }
  
  override func validate() -> (ModelValidationError, String?) {
    if self.uid == nil || self.providerID == nil {
      return (.InvalidId, "uid, providerID")
    }
    
    if self.displayName == nil {
      return (.InvalidBlankAttribute, "displayName")
    }
    
    // Valid
    return (.Valid, nil)
  }
  
  class func allUsersExceptCurrent(completion handler: CollectionQueryResultHandler) {
    MUser.query { (results, error) in
      if error != nil {
        handler(results: [], error: error)
        return
      }
      
      let currentUser = FIRAuth.auth()?.currentUser
      let resultUsers = results as! [MUser]
      let usersExceptCurrent = resultUsers.filter { $0.uid != currentUser?.uid }
      handler(results: usersExceptCurrent, error: nil)
    }
  }
}