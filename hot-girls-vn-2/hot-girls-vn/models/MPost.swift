//
//  Post.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/9/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class MPost: Object {
  dynamic var title: String = ""
  dynamic var likes: Int = 0
  dynamic var viewUrl: String = ""
  dynamic var imageUrl: String = ""
  dynamic var imageWidth: Double = 0
  dynamic var imageHeight: Double = 0
  dynamic var imageData: Data? = nil
  
  dynamic var isFromRealm: Bool = true
  
  let models = List<MModel>()
  
  override class func ignoredProperties() -> [String] {
    return ["isFromRealm"]
  }
  
  override class func primaryKey() -> String? {
    return "imageUrl"
  }
  
  override class func indexedProperties() -> [String] {
    return ["viewUrl", "title"]
  }
  
  func updateProperties(fromAttributes attributes: JSON) -> MPost {
    self.title = attributes["title"].stringValue
    self.likes = attributes["shortLikeCount"].intValue
    self.viewUrl = attributes["viewUrl"].stringValue
    
    let newImageUrl = attributes["widgetImage"].stringValue
    if !newImageUrl.isEmpty {
      let highResUrl = newImageUrl.replacingOccurrences(of: "_wi.jpg", with: "_no.jpg")
      self.imageUrl = "http://photo.depvd.com/\(highResUrl)"
    }
    
    self.imageWidth = attributes["widgetWidth"].doubleValue
    self.imageHeight = attributes["widgetHeight"].doubleValue
    
    for (_, modelDict) in attributes["models"] {
      let modelProfile = MModel.newObject(fromAttributes: modelDict)
      let existedProfile: MModel? = MModel.find(withQuery: "modelUrl == '\(modelProfile.modelUrl)'")
      
      if existedProfile != nil {
        self.models.append(existedProfile!)
      } else {
        self.models.append(modelProfile)
      }
    }
    
    return self
  }
  
  class func newObject(fromAttributes attributes: JSON) -> MPost {
    return MPost().updateProperties(fromAttributes: attributes)
  }
  
  class func find(withQuery queryString: String?) -> MPost? {
    let realm = RealmManager.sharedManager.realmInstance
    var results = realm.objects(MPost.self)
    
    if queryString != nil {
      results = results.filter(queryString!)
    }
    
    return results.first
  }
  
  class func searchPosts(withTitleContains searchKey: String?) -> [MPost] {
    let realm = RealmManager.sharedManager.realmInstance
    var results = realm.objects(MPost.self)
    
    if searchKey != nil {
      results = results.filter("title CONTAINS[c] '\(searchKey!)'")
    }
    
    return [MPost](results)
  }
}
