//
//  RealmModel.swift
//  hot-girls-vn-2
//
//  Created by Ethan Nguyen on 6/21/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import RealmSwift

class RealmModel: Object {
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
