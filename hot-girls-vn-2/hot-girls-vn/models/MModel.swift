//
//  ModelProfile.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/9/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class MModel: Object {
  dynamic var name: String = ""
  dynamic var modelUrl: String = ""
  dynamic var approved: Bool = true
  
  let posts = LinkingObjects(fromType: MPost.self, property: "models")
  
  override static func primaryKey() -> String? {
    return "modelUrl"
  }
  
  override static func indexedProperties() -> [String] {
    return ["modelUrl", "name"]
  }
  
  func updateProperties(fromAttributes attributes: JSON) -> MModel {
    self.name = attributes["name"].stringValue
    self.modelUrl = attributes["modelUrl"].stringValue
    
    // Real value presents not-approvement state, reverse it to store in approved property
    self.approved = !attributes["notApproved"].boolValue
    
    return self
  }
  
  class func find(withQuery queryString: String?) -> MModel? {
    let realm = RealmManager.sharedManager.realmInstance
    var results = realm.objects(MModel.self)
    
    if queryString != nil {
      results = results.filter(queryString!)
    }
    
    return results.first
  }
  
  class func newObject(fromAttributes attributes: JSON) -> MModel {
    return MModel().updateProperties(fromAttributes: attributes)
  }
  
  class func searchPosts(withModelNameContains searchKey: String?) -> [MPost] {
    let realm = RealmManager.sharedManager.realmInstance
    var resultModels = realm.objects(MModel.self)
    
    if searchKey != nil {
      resultModels = resultModels.filter("name CONTAINS[c] '\(searchKey!)'")
    }
    
    var resultPosts = [MPost]()
    
    for resultModel in resultModels {
      resultPosts.append(contentsOf: resultModel.posts)
    }
    
    return resultPosts
  }
}
