//
//  ServiceHelper.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ReachabilitySwift

typealias ReachabilityChangedHandler = (Bool) -> ()

class ServiceHelper: NSObject {
  // Singleton declarations
  static var sharedHelper = ServiceHelper()
  fileprivate override init() {}
  
  // Instance properties
  fileprivate var _reachability: Reachability? = nil
  fileprivate var _reachabilityChangedHandler: ReachabilityChangedHandler? = nil
  
  func getImagesList(page: Int = 1, completion handler: @escaping ([JSON], NSError?) -> ()) {
    let currentTimestamp = Int(Date().timeIntervalSince1970)
    
    let paramPages = [
      // Use current timestamp for first page
      ["lac": "\(currentTimestamp)000", "p.cp": "2"],
      
      // For second pages & so on, use sooner 6 hours timestamp
      ["lac": "\(currentTimestamp-60*60*6)000", "p.cp": "2"],
      ["lac": "\(currentTimestamp-60*60*12)000", "p.cp": "2"]
    ]
    
    // No more data
    if page > paramPages.count {
      handler([], nil)
      return
    }
    
    Alamofire.request("http://www.depvd.com/pajax/topic/list/vn", method: .get, parameters: paramPages[page-1])
      .responseData { (response) in
        switch response.result {
        case .success(let data):
          let jsonObj = JSON(data: data)
          let topics = jsonObj["topics"].arrayValue
          handler(topics, nil)
        case .failure(let error):
          handler([], error as NSError?)
        }
    }
  }
  
  func setupReachability(withHandler handler: @escaping ReachabilityChangedHandler) {
    _reachability = Reachability()!;
    _reachabilityChangedHandler = handler
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(observeReachabilityChanged),
                                           name: ReachabilityChangedNotification,
                                           object: _reachability)
    
    try! _reachability!.startNotifier()
  }
  
  func observeReachabilityChanged(_ notification: Notification) {
    if _reachabilityChangedHandler == nil {
      return
    }
    
    let reachability = notification.object as! Reachability
    let reachableStatus = reachability.isReachable
    _reachabilityChangedHandler!(reachableStatus)
  }
}
