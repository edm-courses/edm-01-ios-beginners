//
//  ViewController.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import SwiftyJSON
import ESPullToRefresh
import KRProgressHUD
import Toaster
import ReachabilitySwift

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
  fileprivate var _rawPostsData: [JSON] = []
  fileprivate var _offlinePostsData: [MPost] = []
  fileprivate var _searchedPostsData: [MPost] = []
  
  fileprivate var _currentPage: Int = 1
  fileprivate var _reachability: Reachability? = nil
  fileprivate var _showOfflinePostsInitially: Bool = true
  fileprivate var _noNetworkError: Bool = false
  fileprivate var _searchingMode: Bool = false
  
  fileprivate var _vPullToRefresh: ESRefreshHeaderView? = nil;
  fileprivate var _vInfiniteScrolling: ESRefreshFooterView? = nil;
  
  @IBOutlet weak var _tblPosts: UITableView!
  @IBOutlet var _vNoNetworkError: UIView!
  @IBOutlet var _vSearchBar: UISearchBar!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupViews()
  }
  
  @IBAction func btnSearchTouchedUpInside(_ sender: UIBarButtonItem) {
    self._toggleSearchBar(showing: !_searchingMode)
  }
  
  // UITableViewDataSource methods
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if _showOfflinePostsInitially {
      return _offlinePostsData.count
    }
    
    if _searchingMode {
      return _searchedPostsData.count
    }
    
    return _rawPostsData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellIdentifier") as! PostCell
    
    if _showOfflinePostsInitially {
      let post = _offlinePostsData[indexPath.row]
      cell.updateCell(withPostModel: post)
    } else if _searchingMode {
      let post = _searchedPostsData[indexPath.row]
      cell.updateCell(withPostModel: post)
    } else {
      let postData = _rawPostsData[indexPath.row]
      cell.updateCell(withData: postData)
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if _noNetworkError {
      Toast(text: "Không có kết nối mạng!\nKhông thể mở nội dung chi tiết", duration: 1).show()
      return
    }
    
    var safariVC: CustomSFSafariViewController? = nil
    
    if _showOfflinePostsInitially {
      let postModel = _offlinePostsData[indexPath.row]
      safariVC = CustomSFSafariViewController(fromUrl: postModel.viewUrl, andTitle: postModel.title)
    } else if _searchingMode {
      let postModel = _searchedPostsData[indexPath.row]
      safariVC = CustomSFSafariViewController(fromUrl: postModel.viewUrl, andTitle: postModel.title)
    } else {
      let postData = _rawPostsData[indexPath.row]
      let viewUrl = postData["viewUrl"].stringValue
      let title = postData["title"].stringValue
      safariVC = CustomSFSafariViewController(fromUrl: viewUrl, andTitle: title)
    }
    
    self.navigationController?.pushViewController(safariVC!, animated: true)
  }
  
  // UITableViewDelegate methods
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    var imageHeight = 0.0
    
    if _showOfflinePostsInitially {
      let post = _offlinePostsData[indexPath.row]
      imageHeight = post.imageHeight
    } else if _searchingMode {
      let post = _searchedPostsData[indexPath.row]
      imageHeight = post.imageHeight
    } else {
      let postData = _rawPostsData[indexPath.row]
      imageHeight = postData["widgetHeight"].doubleValue
    }
    
    return CGFloat(imageHeight * 365 / 192 + 72)
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    let networkErrorHeight = _noNetworkError ? _vNoNetworkError.frame.size.height : 0
    let searchBarHeight = _searchingMode ? _vSearchBar.frame.size.height : 0
    return networkErrorHeight + searchBarHeight
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 10
  }
  
  // UISearchBarDelegate methods
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    
    let searchedPosts = self._searchPosts(withSearchKey: searchBar.text)
    
    // Update new data without re-assign _searchedPostsData variable
    _searchedPostsData.removeAll()
    _searchedPostsData.append(contentsOf: searchedPosts)
    _tblPosts.reloadData()
  }
  
  // Custom functions
  fileprivate func _setupViews() {
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
    
    // Nil search key means all posts
    _offlinePostsData = MPost.searchPosts(withTitleContains: nil)
    
    _vPullToRefresh = _tblPosts.es_addPullToRefresh { [weak self] in
      self?._refreshPostsData()
    }
    
    ServiceHelper.sharedHelper.setupReachability { [weak self] (reachable) in
      self?._toggleNoNetworkError(showing: !reachable)
    }
  }
  
  fileprivate func _refreshPostsData() {
    if _noNetworkError || _searchingMode {
      _tblPosts.es_stopPullToRefresh(ignoreDate: true)
      return
    }
    
    // Reset current showing page
    _currentPage = 1
    
    // Only show HUD at the first time loading data
    if _rawPostsData.count == 0 {
      KRProgressHUD.show(message: "Loading...")
    }
    
    ServiceHelper.sharedHelper.getImagesList { [weak self] (posts, error) in
      self?._tblPosts.es_stopPullToRefresh(ignoreDate: true)
      KRProgressHUD.dismiss()
      
      if error != nil {
        return
      }
      
      self?._rawPostsData.removeAll()
      self?._rawPostsData.append(contentsOf: posts)
      self?._tblPosts.reloadData()
      self?._vInfiniteScrolling = self?._tblPosts.es_addInfiniteScrolling { self?._loadMoreData() }
      RealmManager.sharedManager.insertPosts(posts)
    }
  }
  
  fileprivate func _loadMoreData() {
    if _noNetworkError || _searchingMode {
      _tblPosts.es_stopLoadingMore()
      return
    }
    
    _currentPage += 1
    
    ServiceHelper.sharedHelper.getImagesList(page: _currentPage) { [weak self] (posts, error) in
      self?._tblPosts.es_stopLoadingMore()
      
      if error != nil {
        return
      }
      
      self?._rawPostsData.append(contentsOf: posts)
      self?._tblPosts.reloadData()
      RealmManager.sharedManager.insertPosts(posts)
      
      if posts.count == 0 {
        self?._tblPosts.es_noticeNoMoreData()
      }
    }
  }
  
  fileprivate func _toggleNoNetworkError(showing show: Bool) {
    _noNetworkError = show
    
    // If network is fine, disable show offline posts initially
    if _showOfflinePostsInitially && !_noNetworkError {
      _showOfflinePostsInitially = false
    }
    
    if _noNetworkError {
      _vNoNetworkError.alpha = 0
      var frame = _vNoNetworkError.frame
      frame.origin.y = _tblPosts.frame.origin.y - _vNoNetworkError.bounds.size.height
      _vNoNetworkError.frame = frame
      self.view.addSubview(_vNoNetworkError)
    }
    
    UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
      self?._vNoNetworkError.alpha = show ? 1 : 0
      var frame = self?._vNoNetworkError.frame
      frame?.origin.y = (self?._tblPosts.frame.origin.y)! - (show ? 0 : (frame?.size.height)!)
      self?._vNoNetworkError.frame = frame!
    }) { [weak self] (completed) in
      if completed {
        self?._tblPosts.reloadData()
        
        if show {
          self?._tblPosts.es_noticeNoMoreData()
        } else {
          self?._tblPosts.es_resetNoMoreData()
          self?._vNoNetworkError.removeFromSuperview()
          
          if self?._rawPostsData.count == 0 {
            self?._tblPosts.es_startPullToRefresh()
          }
        }
      }
    }
  }
  
  fileprivate func _toggleSearchBar(showing show: Bool) {
    _searchingMode = show
    
    // Always disable show offline posts when enter searching mode
    if _searchingMode {
      _showOfflinePostsInitially = false
    } else {
      // If searching mode disabled, show offline posts depends on whether network is fine or not
      _showOfflinePostsInitially = _noNetworkError
    }
    
    if _searchingMode {
      _vSearchBar.alpha = 0
      var frame = _vSearchBar.frame
      frame.origin.y = _tblPosts.frame.origin.y - _vSearchBar.bounds.size.height
      _vSearchBar.frame = frame
      self.view.insertSubview(_vSearchBar, belowSubview: _vNoNetworkError)
    }
    
    UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
      self?._vSearchBar.alpha = show ? 1 : 0
      var frame = self?._vSearchBar.frame
      
      if show {
        frame?.origin.y = (self?._vNoNetworkError.frame.origin.y)! + (self?._vNoNetworkError.frame.size.height)!
      } else {
        frame?.origin.y = (self?._tblPosts.frame.origin.y)!
      }
      
      self?._vSearchBar.frame = frame!
    }) { [weak self] (completed) in
      if completed {
        self?._tblPosts.reloadData()
        
        if !show {
          self?._vSearchBar.removeFromSuperview()
        }
      }
    }
  }
  
  fileprivate func _searchPosts(withSearchKey searchKey: String?) -> [MPost] {
    if searchKey == nil {
      return []
    }
    
    let directResults = MPost.searchPosts(withTitleContains: searchKey)
    let viaModelResults = MModel.searchPosts(withModelNameContains: searchKey)
    let uniqueResults = Set<MPost>(directResults + viaModelResults)
    
    return [MPost](uniqueResults)
  }
}
