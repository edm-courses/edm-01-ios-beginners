//
//  PostCell.swift
//  hot-girls-vn
//
//  Created by Ethan Nguyen on 6/7/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON

class PostCell: UITableViewCell {
  @IBOutlet weak var _imgPostImage: UIImageView!
  @IBOutlet weak var _lblTitle: UILabel!
  @IBOutlet weak var _lblLikes: UILabel!
  
  // From raw data
  func updateCell(withData data: JSON) {
    if data.isEmpty {
      return
    }
    
    _lblTitle.text = data["title"].stringValue
    _lblLikes.text = data["shortLikeCount"].stringValue
    self._loadImage(fromData: data)
  }
  
  // From post model in Realm
  func updateCell(withPostModel post: MPost) {
    if post.imageData != nil {
      _imgPostImage.image = UIImage(data: post.imageData! as Data)
    } else {
      _imgPostImage.af_setImage(withURL: URL(string: post.imageUrl)!)
    }
    
    _lblTitle.text = post.title
    _lblLikes.text = String(post.likes)
  }
  
  fileprivate func _loadImage(fromData data: JSON) {
    let widgetImage = data["widgetImage"].stringValue
    let imagePath = widgetImage.replacingOccurrences(of: "_wi.jpg", with: "_no.jpg")
    let imageUrl = URL(string: "http://photo.depvd.com/\(imagePath)")
    
    _imgPostImage.image = nil
    _imgPostImage.af_setImage(withURL: imageUrl!, imageTransition: .crossDissolve(0.5))
  }
}
