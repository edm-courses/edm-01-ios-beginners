//
//  CustomSFSafariViewController.swift
//  news-feed-reader
//
//  Created by Ethan Nguyen on 6/11/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import Foundation
import SafariServices

class CustomSFSafariViewController: SFSafariViewController {
  fileprivate var _postTitle: String? = nil
  fileprivate var _oldNavTitleTextAttr: [String: AnyObject]? = nil
  
  convenience init(fromUrl viewUrl: String, andTitle title: String) {
    self.init(url: URL(string: "http://www.depvd.com/\(viewUrl)")!)
    _postTitle = title
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self._setupNavigationBar()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    _oldNavTitleTextAttr = self.navigationController?.navigationBar.titleTextAttributes as [String : AnyObject]?
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Light", size: 15)!
    ]
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.navigationController?.navigationBar.titleTextAttributes = _oldNavTitleTextAttr
  }
  
  // Custom functions
  fileprivate func _setupNavigationBar() {
    if self.navigationController == nil {
      return
    }
    
    self.navigationItem.title = _postTitle!
    self.navigationItem.hidesBackButton = true
    let customBackButton = UIBarButtonItem(image: UIImage(named: "btn-back")!,
                                           style: .plain,
                                           target: self.navigationController!,
                                           action: #selector(UINavigationController.popViewController(animated:)))
    self.navigationItem.leftBarButtonItem = customBackButton
  }
}
