//
//  BaseViewController.swift
//  taxi-fare
//
//  Created by Ethan Nguyen on 6/8/16.
//  Copyright © 2016 Thanh Nguyen. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSFontAttributeName : UIFont(name: "Roboto-Medium", size: 17)!
    ]
  }
}
